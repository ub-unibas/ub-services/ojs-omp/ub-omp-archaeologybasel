/**
 * @author alexander.farkas
 */
/**
 * JS-Singelton-Klasse um Objekte (zum Beispiel Bilder) zu skalieren
 * @id objScaleModule
 * @alias $.objScale
 * @alias jQuery.objScale
 */

/**
 * Berechnet die Höhe und Breite von DOM-Objekten
 * 
 * @id getDim
 * @method
 * @alias $.objScale.getDim
 * @param {Object} obj obj erwartet ein DOM-Objekt, ein jQuery-Objekt oder ein Objekt mit den Eigenschaften width und height.
 * @return {Object} gibt ein Objekt mit höhe und Breite zurück. Beispiel. {height: 200, width: 300}
 */
/**
 * Berechnet eine verhältnismäßige Skalierung eines Objekts.<br>
 * siehe auch: $.objScale.scaleHeightTo und $.objScale.scaleWidthTo
 * 
 * @id scaleTo
 * @method
 * @alias $.objScale.scaleTo
 * @see #scaleHeightTo
 * @param {Object} obj obj erwartet ein DOM-Objekt, ein jQuery-Objekt oder ein Objekt welches skaliert werden soll.
 * @param {Number} num num erwartet die neue Grösse, welche das Objekt haben soll (Breite oder Höhe)
 * @param {String} side gibt an welche Seite (Höhe oder Breite) man im 2. Parameter angegeben hat
 * @return {Number} gibt die neue Länge zurück (Wenn man unter num/side 'width' angegeben hat, wird die Höhe zurückgeliefert).
 */
/**
 * Skaliert die Höhe eines Objekts und gibt die verhältnismäßige Breite zurück.<br> 
 * (Shorthand für $.objScale.scaleTo(obj, num, 'height');)
 * 
 * @id scaleHeightTo
 * @method
 * @alias $.objScale.scaleHeightTo
 * @param {Object} obj obj erwartet ein DOM-Objekt, ein jQuery-Objekt oder ein Objekt welches skaliert werden soll.
 * @param {Number} num num erwartet die neue Höhe, welche das Objekt haben soll
 * @return {Number} gibt die neue Breite zurück
 */
/**
 * Skaliert die Breite eines Objekts und gibt die verhältnismäßige Höhe zurück.<br> 
 * (Shorthand für $.objScale.scaleTo(obj, num, 'width');)
 * 
 * @id scaleWidthTo
 * @method
 * @alias $.objScale.scaleWidthTo
 * @param {Object} obj obj erwartet ein DOM-Objekt, ein jQuery-Objekt oder ein Objekt welches skaliert werden soll.
 * @param {Number} num num erwartet die neue Breite, welche das Objekt haben soll
 * @return {Number} gibt die neue Höhe zurück
 */

/**
 * Skaliert die Breite eines Objekts und gibt die verhältnismäßige Höhe zurück.<br> 
 * (Shorthand für $.objScale.scaleTo(obj, num, 'width');)
 * 
 * @id scaleWidthTo
 * @method
 * @alias $.objScale.scaleWidthTo
 * @param {Object} obj obj erwartet ein DOM-Objekt, ein jQuery-Objekt oder ein Objekt welches skaliert werden soll.
 * @param {Number} num num erwartet die neue Breite, welche das Objekt haben soll
 * @return {Number} gibt die neue Höhe zurück
 */
/**
 * Zentriert ein kleineres Objekt in einem Grösseren.<br> 
 * siehe auch: $.objScale.constrainObjTo();
 * 
 * @id centerObjTo
 * @method
 * @alias $.objScale.centerObjTo
 * @param {Object} obj obj erwartet ein DOM-Objekt, ein jQuery-Objekt oder ein anderes Objekt mit Höhen und Breiten-Eigenschaften,  welches zentriert werden soll.
 * @param {Object} container erwartet ein DOM-Objekt, ein jQuery-Objekt oder ein anderes Objekt mit Höhen und Breiten-Eigenschaften,  in welches das andere Objekt zentriert werden soll
 * @param {Object, Options} opts stellt Optionen bereit so kann angegeben werden, ob es einen Mindest nach oben bzw. links geben soll (margin: [10, false]) und, ob vertical und / oder horizontal zentriert werden soll<br><br>
 * Beispiel:<br>
 * $.objScale.centerObjTo(img, container, {margin: [10, 0], horizontal: false};<br>
 * Es soll nur vertical und nicht horizontal zentriert werden, ausserdem soll der Mindestabstand nach oben 10 Einheiten betragen<br><br>
 * $.objScale.centerObjTo(img, container, {margin: [false, 0]};<br>
 * Es soll vertical und horizontal zentriert werden, ausserdem soll der Mindestabstand nach links 0 Einheiten betragen und nach oben existiert keine Mindestbeschränkung (Es können negative Werte auftreten).<br><br>
 * defaults: {margin: [0, 0], vertical: true, horizontal: true}
 * @return {Object} gibt ein Objekt mit top und left Eigenschaften zurück
 */
/**
 * Zentriert ein Objekt in einem anderen Objekt. Ist das zu skalierende Objekt grösser, wird es zusätzlich verkleinert.<br> 
 * siehe auch: $.objScale.centerObjTo(); und $.objScale.scaleObjTo();
 * @id constrainObjTo
 * @method
 * @alias $.objScale.constrainObjTo
 * @param {Object} obj obj erwartet ein DOM-Objekt, ein jQuery-Objekt oder ein anderes Objekt mit Höhen und Breiten-Eigenschaften,  welches angepasst und zentriert werden soll.
 * @param {Object} container erwartet ein DOM-Objekt, ein jQuery-Objekt oder ein anderes Objekt mit Höhen und Breiten-Eigenschaften,  in welches das andere Objekt zentriert werden soll
 * @param {Object, Options} opts stellt Optionen bereit so kann angegeben werden, ob es einen Mindestabstand nach oben bzw. links geben soll (margin: [10, false], padding: [10, 0]) und ob vertical und / oder horizontal zentriert werden soll<br><br>
 * Unterschied zwischen padding und margin: Die margin- und padding-Angaben werden für die evtl. Verkleinerung des Objekts berücksichtigt. Bei einer möglichen Zentrierung wird dagegen ausschließlich der margin-Wert berücksichtigt. Das padding-Array darf daher nur Zahlen enthalten, das margin-Array darf daneben noch den booleschen Wert false enthalten.
 * Beispiel:<br>
 * $.objScale.constrainObjTo(img, container, {margin: [10, 0], horizontal: false};<br>
 * Es soll nur vertical und nicht horizontal zentriert werden, ausserdem soll der Mindestabstand nach oben 10 Einheiten betragen<br><br>
 * defaults: {margin: [0, 0], padding: [0, 0], vertical: true, horizontal: true}
 * @return {Object} gibt ein Objekt mit width, height, top und left Eigenschaften zurück
 */
/**
 * Skaliert ein Objekt, so dass es perfekt in ein anderes Objekt passt und zentriert es. (Ist es kleiner, wird es vergrössert bzw. ist grösser, wird es verkleinert).<br> 
 * siehe auch: $.objScale.centerObjTo(); und $.objScale.constrainObjTo();
 * @id scaleObjTo
 * @method
 * @alias $.objScale.scaleObjTo
 * @param {Object} obj obj erwartet ein DOM-Objekt, ein jQuery-Objekt oder ein anderes Objekt mit Höhen und Breiten-Eigenschaften,  welches skaliert und zentriert werden soll.
 * @param {Object} container erwartet ein DOM-Objekt, ein jQuery-Objekt oder ein anderes Objekt mit Höhen und Breiten-Eigenschaften,  in welches das andere Objekt zentriert/skaliert werden soll
 * @param {Object, Options} opts stellt Optionen bereit so kann angegeben werden, ob es einen Mindestabstand nach oben bzw. links geben soll (margin: [10, false], padding: [10, 0]), ob vertical und / oder horizontal zentriert werden soll. Die scaleToFit-Eigenschaft gibt an, ob bei unterschiedlichen Seitenverhältnissen das innere Objekt vollständig zu sehen sein soll oder ob es das äußere Objekt vollständig ausfüllen soll<br><br>
 * Unterschied zwischen padding und margin: Die margin- und padding-Angaben werden für die evtl. Skalierung des Objekts berücksichtigt. Bei einer möglichen Zentrierung wird dagegen ausschließlich der margin-Wert berücksichtigt. Das padding-Array darf daher nur Zahlen enthalten, das margin-Array darf daneben noch den booleschen Wert false enthalten.
 * Beispiel:<br>
 * $.objScale.scaleObjTo(img, container, {margin: [10, 0], horizontal: false};<br>
 * Es soll nur vertical und nicht horizontal zentriert werden, ausserdem soll der Mindestabstand nach oben 10 Einheiten betragen<br><br>
 * defaults: {margin: [false, false], padding: [0, 0], scaleToFit: false, vertical: true, horizontal: true}
 * @return {Object} gibt ein Objekt mit width, height, top und left Eigenschaften zurück
 */
(function($) {
	/**
	 * @id objScaleModule
	 */
	$.objScale = (function() {

		/**
		 * @id getDim
		 */
		function getDim(obj) {
			var ret;
			if (obj.nodeName) {
				obj = $(obj);
			} else if (isFinite(obj.width) && isFinite(obj.height)) {
				ret = {
					width: obj.width,
					height: obj.height
				};
			}
			if (!ret) {
				if (obj.is('object')) {
					ret = {
						height: parseInt(obj[0].height, 10),
						width: parseInt(obj[0].width, 10)
					};
				} else {
					ret = {
						height: obj.height(),
						width: obj.width()
					};
					if (!ret.height && !ret.width && 'naturlaWidth' in obj[0]) {
						ret = {
							height: obj[0].naturlaHeight,
							width: obj[0].naturlaWidth
						};
					}
				}
			}

			return ret;
		}

		/**
		 * @id scaleTo
		 */
		function scaleTo(obj, num, side) {
			var cur = getDim(obj),
				percentage,
				reverseSide = (side == 'height') ?
				'width' :
				'height';

			percentage = cur[side] / num;
			return cur[reverseSide] / percentage;
		}

		/**
		 * @id scaleHeightTo
		 */
		function scaleHeightTo(obj, height) {
			return scaleTo(obj, height, 'height');
		}

		/**
		 * @id scaleWidthTo
		 */
		function scaleWidthTo(obj, width) {
			return scaleTo(obj, width, 'width');
		}

		/**
		 * @id constrainObjTo
		 */
		function constrainObjTo(obj, container, opts) {
			opts = $.extend({
				margin: [0, 0],
				padding: [0, 0],
				cleanCSS: true
			}, opts);
			var cur = getDim(obj),
				con = getDim(container),
				maxWidth = con.width - opts.padding[1],
				maxHeight = con.height - opts.padding[0],
				estimatetPer = con.height / con.width,
				curPer = cur.height / cur.width,
				ret = $.extend({}, cur);

			if (opts.margin[1]) {
				maxWidth -= opts.margin[1] * 2;
			}
			if (opts.margin[0]) {
				maxHeight -= opts.margin[0] * 2;
			}
			if (opts.minWidth) {
				maxWidth = Math.max(opts.minWidth, maxWidth);
			}
			if (opts.minHeight) {
				maxHeight = Math.max(opts.minHeight, maxHeight);
			}
			if (estimatetPer < curPer && maxHeight < cur.height) {
				ret.width = scaleTo(obj, maxHeight, 'height');
				ret.height = maxHeight;
			} else if (maxWidth < cur.width) {
				ret.width = maxWidth;
				ret.height = scaleTo(obj, maxWidth, 'width');
			}
			if (!opts.cleanCSS) {
				ret.widthSubtraction = ret.width - cur.width;
				ret.heightSubtraction = ret.height - cur.height;
			}
			$.extend(ret, centerObjTo(ret, con, opts));
			return ret;
		}

		/**
		 * @id centerObjTo
		 */
		function centerObjTo(obj, container, opts) {
			opts = $.extend({
				margin: [0, 0],
				vertical: true,
				horizontal: true
			}, opts);
			var cur = getDim(obj),
				con = getDim(container),
				ret = {};

			if (opts.vertical) {
				ret.top = (con.height - cur.height) / 2;
				if (isFinite(opts.margin[0])) {
					ret.top = Math.max(ret.top, opts.margin[0]);
				}
			}

			if (opts.horizontal) {
				ret.left = (con.width - cur.width) / 2;
				if (isFinite(opts.margin[1])) {
					ret.left = Math.max(ret.left, opts.margin[1]);
				}
			}
			return ret;
		}

		/**
		 * @id scaleObjTo
		 */
		function scaleObjTo(obj, container, opts) {
			opts = $.extend({
				margin: [false, false],
				padding: [0, 0],
				scaleToFit: false
			}, opts);

			var cur = getDim(obj),
				con = getDim(container),
				curPer = cur.height / cur.width,
				ret = {};

			con.maxHeight = con.height - opts.padding[0];
			con.maxWidth = con.width - opts.padding[1];

			if (opts.margin[0]) {
				con.maxHeight -= opts.margin[0];
			}
			if (opts.margin[1]) {
				con.maxWidth -= opts.margin[1];
			}

			var estimatetPer = con.maxHeight / con.maxWidth;

			if (opts.scaleToFit !== estimatetPer > curPer) {
				ret.width = con.maxWidth;
				ret.height = scaleTo(obj, con.maxWidth, 'width');
			} else {
				ret.width = scaleTo(obj, con.maxHeight, 'height');
				ret.height = con.maxHeight;
			}

			$.extend(ret, centerObjTo(ret, con, opts));
			return ret;
		}

		return {
			scaleWidthTo: scaleWidthTo,
			scaleHeightTo: scaleHeightTo,
			scaleSidesIn: scaleObjTo,
			/* dep */
			scaleObjTo: scaleObjTo,
			constrainObjTo: constrainObjTo,
			getDim: getDim,
			centerObjTo: centerObjTo
		};
	})();
})(window.webshims && webshims.$ || jQuery);
