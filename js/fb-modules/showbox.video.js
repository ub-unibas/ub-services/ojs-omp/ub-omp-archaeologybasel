/**
 * Author: fr
 */
(function($) {
	var videoDefaults = {
		dims: {
			width: 600,
			height: 300
		},
		attrs: {
			controls: 'controls'
		},
		removeObject: true,
		vidWrapper: '<div class="mediaplayer" data-jme=\'{"controlbar": true}\'></div>'
	};
	if ($.fn.showbox) {
		$.fn.showbox.defaults.video = videoDefaults;
	}

	var vidReg = /\.mp4$|\.webm|\.mov$|\.avi$|\.ogg$|\.m4v$|\.flv$/i;
	$.createUrlIndex.mmContent.add('video', {
		filter: function(url, opener, urlPart, type) {
			if (type[1][0] === 'video' || opener.is('.vid, .video')) {
				return true;
			}
			return (vidReg.test(urlPart));
		},
		load: function(url, opener, ui, fn) {
			var inst = ui.instance || ui,
				jElm = $([]),
				opts = inst.options.video,
				elmOpts = opener.data('showbox') || {},
				attrs = $.extend({}, opts.attrs, elmOpts.attrs || {}),
				dims = $.extend({}, opts.dims);

			if (elmOpts.height) {
				dims.height = parseInt(elmOpts.height, 10);
			}
			if (elmOpts.width) {
				dims.width = parseInt(elmOpts.width, 10);
			}

			inst.content = inst.content || {};

			if (ui.extras) {
				ui.extras.mm = jElm;
			}

			if (!attrs.poster) {
				attrs.poster = $('img', opener).attr('src');
			}
			if (!attrs.poster && 'poster' in attrs) {
				delete attrs.poster;
			}

			inst.content = {
				'multimedia-box': function(name, element, content, isClone) {
					var elem = $(opts.vidWrapper).css(dims);
					var src, type;
					$('div.multimedia-box', element).html(elem);

					if (ui.extras) {
						ui.extras.mm = elem;
					}
					if (!isClone) {
						src = $('<source />').prop('src', url);
						type = opener.attr('type');

						if (type) {
							src.prop({
								type: type
							});
						}
						// video element will be processed with polyfill in ui.cOverlay
						jElm = $(document.createElement('video'))
							.attr(attrs)
							.css({
								width: '100%',
								height: '100%'
							});

						jElm.append(src);

						jElm.appendTo(elem);
						elem.updatePolyfill();
					}
					return elem;
				}
			};
			inst.content['multimedia-box'][0] = dims;
			$.extend(inst.content['multimedia-box'], dims);


			fn(url, dims.width);
		}
	}, videoDefaults);
})(window.webshims && webshims.$ || jQuery);
