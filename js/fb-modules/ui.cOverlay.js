/**
 * @author trixta
 */
(function($) {

	/* Mask */

	var maskID = new Date().getTime();

	$.widget('ui.overlayProto', {
		hideElementsOnShow: function() {
			var o = this.options,
				that = this;

			this.hiddenElements = $([]);

			if (o.hideWindowedFlash) {
				this.hiddenElements = $('object, embed').filter(function() {
					return !(
						((this.getAttribute('classid') || '').toLowerCase() === 'clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' || this.getAttribute('type') === 'application/x-shockwave-flash') && //isFlash
						(this.getAttribute('transparent') !== 'transparent' &&
							(/<param\s+(?:[^>]*(?:name=["'?]\bwmode["'?][\s\/>]|\bvalue=["'?](?:opaque|transparent)["'?][\s\/>])[^>]*){2}/i.test(this.innerHTML)))
					);
				});
			}

			if (o.hideWhileShown) {
				this.hiddenElements = this.hiddenElements.add(o.hideWhileShown);
			}

			this.hiddenElements = this.hiddenElements
				.filter(function() {

					return ($.css(this, 'visibility') !== 'hidden' && !$.contains(that.element[0], this));
				})
				.filter(o.hideFilter)
				.css({
					visibility: 'hidden'
				});
		}
	});

	$.widget('ui.mask', $.ui.overlayProto, {
		options: {
			extraClass: false,
			closeOnClick: true,
			closeOnEsc: true,
			hideFilter: function() {
				return true;
			},
			handleDisplay: true,
			fadeInTime: 0,
			fadeOutTime: 0,
			opacity: 0.8,
			bgIframe: false
		},
		_create: function() {
			var o = this.options,
				that = this,
				css;
			maskID++;
			this.id = maskID;
			this.maskedElement = this.element.parent();

			if (this.maskedElement.is('body')) {
				this.dimensionElement = $(document);
				this.calcMethod = {
					height: 'height',
					width: 'width'
				};
			} else {
				this.dimensionElement = this.maskedElement.css({
					position: 'relative'
				});
				this.calcMethod = {
					height: 'innerHeight',
					width: 'innerWidth'
				};
			}
			css = {
				top: 0,
				left: 0,
				right: 0,
				bottom: 0
			};

			this.calcSize = false;

			css.display = 'none';
			css.position = 'absolute';

			if (this.maskedElement.is('body')) {
				css.position = 'fixed';
			}

			this.element.css(css);
			this.isVisible = false;
			if (o.closeOnClick) {
				this.element.click(function(e) {
					that.hide.call(that, e, this);
				});
			}
			if (o.extraClass) {
				this.element.addClass(o.extraClass);
			}
		},
		ui: function() {
			return {
				instance: this
			};
		},
		hide: function(e, elem) {

			if (!this.isVisible) {
				return;
			}
			var result = this._trigger('close', e, this.ui()),
				o = this.options,
				that = this;
			if (result === false) {
				return;
			}
			this.isVisible = false;

			if (o.handleDisplay) {
				if (o.fadeOutTime) {
					this.element.fadeOut(o.fadeOutTime, function() {
						that.unexpose.call(that);
					});
				} else {
					this.element.hide();
					this.unexpose();
				}
			}
			this.element.queue(function() {
				if (that.hiddenElements && that.hiddenElements.css) {
					that.hiddenElements.css({
						visibility: 'visible'
					});
				}
				that.maskedElement.removeClass('mask-visible');
				that.element.dequeue();
			});
			$(document).unbind('.mask' + this.id);
			$(window).unbind('.mask' + this.id);
		},
		resize: function(set) {
			var ret = {
				'height': this.dimensionElement[this.calcMethod.height]()
			};
			if (!this.options.cssWidth) {
				ret.width = this.dimensionElement[this.calcMethod.width]();
			}
			if (set && this.calcSize) {
				this.element.css(ret);
			}
			return ret;
		},
		show: function(e, o) {

			if (this.isVisible) {
				return;
			}
			o = (o) ? $.extend(true, {}, this.options, o) : this.options;
			var that = this,
				resize = function(e) {
					that.resize.call(that, true);
				};
			if (o.expose) {
				this.expose(o.expose);
			}

			this._trigger('show', e, $.extend(true, {}, this.ui(), o));
			this.isVisible = true;
			this.maskedElement.addClass('mask-visible');
			this.hideElementsOnShow();
			if (o.handleDisplay) {
				if (this.calcSize) {
					this.resize(true);
				}
				if (o.fadeInTime) {
					this.element.fadeInTo(o.fadeInTime, o.opacity);
				} else {
					this.element.css({
						opacity: o.opacity,
						display: 'block'
					});
				}
			}

			if (o.closeOnEsc) {
				$(document).bind('keydown.mask' + this.id, function(e) {
					if (e.keyCode === $.ui.keyCode.ESCAPE) {
						that.hide.call(that, e, this);
					}
				});
			}
			if (that.calcSize) {
				$(document).bind('resize.mask' + this.id + ' emchange.mask' + this.id, resize);
				$(window).bind('resize.mask' + this.id, resize);
			}
		},
		unexpose: function(elem) {
			if (!elem && !this.exposed) {
				return;
			}
			var exposed = elem || this.exposed;
			exposed.each(function() {
				$(this).css({
					position: '',
					zIndex: ''
				});
			});
			if (!elem) {
				this.exposed = false;
			}
		},
		expose: function(jElm) {
			var zIndex = parseInt(this.maskedElement.css('z-index'), 10) || 9;
			jElm = this.maskedElement.find(jElm);
			jElm.each(function() {
				var jExpose = $(this);
				if (jExpose.css('position') === 'static') {
					jExpose.css({
						position: 'relative'
					});
				}
				zIndex++;
				jExpose.css({
					zIndex: zIndex
				});
			});
			this.exposed = jElm;
		}
	});

	/* END: Mask */

	/* Content-Overlay */

	var currentFocus,
		id = new Date().getTime();

	$(document).bind('focusin', function(e) {
		if (e.target.nodeType == 1) {
			currentFocus = e.target;
		}
	});

	if (!$.fn.mask) {
		$.fn.mask = function() {
			return this;
		};
	}

	$.widget('ui.cOverlay', $.ui.overlayProto, {
		options: {
			//
			mask: false, //Soll die Seite zusätzlich maskiert werden
			maskOpts: {}, //Optionen für die Maskierung, siehe mask-Plugin im Overlay-Ordner
			hideStyle: 'visibility',
			bgIframe: false, //IE6 bugfix für select-zIndex-Bug
			hideWindowedFlash: 'auto', // Sollen Flashelemente versteckt werden, die kein wmode haben
			hideWhileShown: false, // Selektor von Elementen, DOM-Objekte die während der Anzeige versteckt werden sollen
			hideFilter: function() {
				return true;
			}, // funktion zum herausfiltern von Objekten die versteckt werden sollen

			extraClass: false, // Zusatzklasse für Overlay-Element
			attrs: {}, //zusätzliche Attribute, für das Overlay-Element
			bodyShowClass: 'overlay-visible', //body-Klasse die gesetzt ist solange das Overlay angezeigt wird (gut für Print-Stylesheets)

			positionType: 'centerInsideView', // Name der Funktion im Namespace  $.ui.cOverlay.posMethods bzw die Funktion selbst, welche die Position berechnet
			positionOpts: {}, //optionen der positions-Funktion
			//mögliche weitere Positionierungs-Optionen
			followMouse: false,

			restoreFocus: 'auto', // Ob der Focus beim Schliessen auf das Element gesetzt werden soll, welches vor dem öffnen fokusiert war, bei auto ist dies true, sofern focusOnShow gesetzt wurde
			focusOnShow: false, // Ob das Overlay fokusiert werden soll, wenn es geöffnet wird. Wird ein Selektor angegeben, dann wird dieses Element fokusiert

			closeOnEsc: true,
			closeBtnSel: 'a.close-button',

			animShow: function(jElm, data) { //Show-Animation (ui.posCSS enthält die berechnete Positionierung und muss gesetzt werden) 
				var showStyle = (data.instance.options.hideStyle == 'visibility') ? {
					visibility: 'visible'
				} : {
					display: 'block'
				};
				jElm.css(data.posCSS).css(showStyle);
			},
			animHide: function(jElm, data) { //Hide-Animation 
				var hiddenStyle = (data.instance.options.hideStyle == 'visibility') ? {
					visibility: 'hidden'
				} : {
					display: 'none'
				};
				jElm.css(hiddenStyle);
			},

			addRole: false, // nonfocussyle: tooltip || alert || focusstyle: alertdialog || dialog 
			createA11yWrapper: 'auto',
			labelledbySel: false,
			describedbySel: false,

			//Opener
			openerSel: false, // Elemente (Selektor:String, jQuery:Object, DOM:Object), welche das Overlay öffnen
			openerContext: document, // Kontext (DOM:Object, jQuery:Object) in dem nach openerSel gesucht wird
			bindStyle: 'bind', // Art des Event-Bindings (bind|live)

			//opencloseEvents werden durch a11ymode erweitert
			openEvent: 'ariaclick', // mouseenter || click
			closeEvent: false,
			openDelay: 1, //Zeit die vergehen soll bis das overlay geöffnet wird
			closeDelay: 1,
			setInitialContent: false,
			handleElementEnterLeave: false
		},
		customEvents: ['init', 'create', 'beforeshow', 'show', 'beforehide', 'hide'],
		_create: function() {
			this._getMarkupOptions();
			var o = this.options,
				that = this,
				close = function(e) {
					var elem = this;

					that.timer.clear('openTimer');

					that.timer.setDelay('closeTimer', function() {
						that.hide(e, {
							closer: elem
						});
					}, o.closeDelay);
					return false;
				},
				show = function(e) {
					var elem = this;
					if (that.closeTimer !== undefined && (!that.currentOpener || that.currentOpener[0] === elem || elem === that.element[0])) {
						that.timer.clear('closeTimer');
					}
					that.timer.setDelay('openTimer', function() {
						that.show(e, {
							opener: elem
						});
					}, o.openDelay);
					return false;
				};
			var hideCss = (o.hideStyle === 'visibility') ? {
				visibility: 'hidden'
			} : {
				display: 'none'
			};
			this.element
				.css(hideCss)
				.attr(o.attrs)
				.attr({
					'aria-hidden': 'true'
				});



			if (o.openDelay < 1) {
				o.openDelay = 1;
			}
			this.timer = $.createTimer(this);
			this.mask = $([]);

			if (o.mask && o.hideWindowedFlash === 'auto') {
				o.maskOpts = o.maskOpts || {};
				o.hideWindowedFlash = true;
			} else {
				o.hideWindowedFlash = false;
			}

			if (o.maskOpts) {
				o.maskOpts.hideWindowedFlash = false;
			}
			if (o.extraClass) {
				this.element.addClass(o.extraClass);
			}


			if (o.restoreFocus === 'auto') {
				o.restoreFocus = !!(o.focusOnShow);
			}

			id++;
			this.id = 'overlay-' + id;
			this.isVisible = false;
			this.hiddenElements = $([]);
			this.openers = $([]);

			if (o.handleElementEnterLeave) {
				this.element.enterLeave(
					function() {
						that.timer.clear('closeTimer');
					},
					close, typeof o.handleElementEnterLeave == 'object' ? o.handleElementEnterLeave : {});
			}

			if (o.openerSel) {
				this.openers = $(o.openerSel, o.openerContext);
				if (o.openEvent) {
					$.Aperto.onType(o.bindStyle, this.openers, o.openEvent, show);
				}
				if (o.closeEvent) {
					$.Aperto.onType(o.bindStyle, this.openers, o.closeEvent, close);
				}
			}

			this._trigger('init', {
				type: 'init'
			}, this.ui());
		},
		_lazyAddDocument: function() {
			if (this.isFullInitialized) {
				return;
			}
			this.isFullInitialized = true;
			var o = this.options;

			var that = this;


			if (o.setInitialContent) {
				this.fillContent(this.element, o.setInitialContent);
			}

			this.clonedOverlay = this.element.clone().attr({
				role: 'presentation'
			}).addClass('cloned-overlay');

			this.closeBtn = $(o.closeBtnSel, this.element);

			this.element.on('click', o.closeBtnSel, function(e) {
				that.timer.clear('openTimer');
				that.hide(e, {
					closer: this
				});
				return false;
			});


			if ($.support.waiAria) {
				if (this.closeBtn[0] && $.nodeName(this.closeBtn[0], 'a')) {
					this.closeBtn.attr({
						tabindex: '0',
						role: 'button',
						'aria-valuenow': ''
					});
				}

				if (o.labelledbySel) {
					this.element.labelWith($(o.labelledbySel, this.element));
				}

				if (o.describedbySel) {
					this.element.describeWith($(o.describedbySel, this.element));
				}
				if (o.addRole) {
					this.element.attr('role', o.addRole);
				}
			}
			if (!this.element.parent()[0]) {
				this.element.appendTo('body');
			}
			if (o.mask) {
				this.mask = $('<div class="mask" />')
					.insertBefore(this.element)
					.mask(
						$.extend(o.maskOpts, {
							close: function(e, ui) {
								that.timer.clear('openTimer');
								return that.hide(e, ui);
							}
						})
					);
			}
			if (o.createA11yWrapper === true || (o.createA11yWrapper && this.element.parent().is('body'))) {
				var a11yWrapper = this.element.siblings('.a11y-wrapper');
				if (a11yWrapper[0]) {
					this.element.appendTo(a11yWrapper[0]);
				} else {
					this.element.wrap('<div class="a11y-wrapper" />');
				}
			}
		},
		fillContent: function(element, content, isClone) {
			var o = this.options;

			element = element || this.element;
			content = content || this.content || {};
			$.each(content, function(name, html) {
				if ($.isFunction(html)) {
					html(name, element, content, isClone);
				} else {
					$('.' + name, element).html(html);
				}
			});
			if (o.addRole === 'tooltip' || o.addRole === 'alert') {
				$('*', this.element).attr({
					role: 'presentation'
				});
			}
		},
		ui: function() {
			var obj = {
					instance: this,
					isVisible: this.isVisible,
					openers: this.openers,
					id: this.id,
					element: this.element
				},
				arg = arguments;

			for (var i = 0, len = arg.length; i < len; i++) {
				if (arg[i]) {
					$.extend(obj, arg[i]);
				}
			}
			return obj;
		},
		show: function(e, extras) {
			this._lazyAddDocument();
			this.timer.clear('closeTimer');
			this.currentOpener = (extras && extras.opener) ? $(extras.opener) : (e && e.currentTarget) ? $(e.currentTarget) : $(currentFocus);
			extras = extras || {};
			extras.opener = this.currentOpener;

			if (this.isVisible || this._trigger('beforeShow', e, this.ui({
					extras: extras
				})) === false || this.stopShow) {
				return;
			}
			this.isVisible = true;
			var o = this.options,
				that = this,
				posCSS,
				ui;
			this.hideElementsOnShow();
			if (o.addRole === 'tooltip' && this.currentOpener) {
				this.currentOpener.attr({
					'aria-describedby': this.element.getID()
				});
			}
			posCSS = this.setPosition(e, extras);

			ui = this.ui({
				extras: extras,
				posCSS: posCSS
			});
			this.mask.mask('show');

			o.animShow(this.element.stop(), ui);

			this.element.attr({
				'aria-hidden': 'false'
			});


			this.restoreFocus = currentFocus;
			if (o.focusOnShow) {
				if (o.focusOnShow === true) {
					this.element.firstExpOf('focusPoint').setFocus({
						context: (this.element[0].parentNode || {}).parentNode
					});
				} else {
					$(o.focusOnShow, this.element).setFocus({
						context: (this.element[0].parentNode || {}).parentNode
					});
				}
			}

			$('body').addClass(o.bodyShowClass);

			if (o.closeOnEsc) {
				$(document).bind('keydown.' + this.id, function(e) {
					if (e.keyCode === $.ui.keyCode.ESCAPE) {
						that.hide.call(that, e, {
							closer: this
						});
					}
				});
			}

			this.mask.mask('resize', true);
			this._trigger('show', e, ui);
		},
		hide: function(e, extras) {
			if (!this.isVisible) {
				return;
			}
			var o = this.options,
				ui = this.ui({
					extras: extras
				});
			if (this._trigger('beforeHide', e, ui) === false) {
				return false;
			}

			this.isVisible = false;

			if (o.addRole === 'tooltip' && this.currentOpener) {
				this.currentOpener.removeAttr('aria-describedby');
			}


			this.mask.mask('hide');

			$(document).unbind('.' + this.id);
			$(window).unbind('.' + this.id);
			if (o.restoreFocus && this.restoreFocus) {
				$(this.restoreFocus).setFocus({
					fast: true
				});
			}

			o.animHide(this.element, ui);
			if (this.removeFlashContent) {
				this.removeFlashContent();
			}
			this.element.attr({
				'aria-hidden': 'true'
			});

			this.hiddenElements.css({
				visibility: 'visible'
			});
			this._trigger('hide', e, ui);
			$('body').removeClass(o.bodyShowClass);
			this.restoreFocus = false;
		},
		setPosition: function(e, extras, elem) {

			elem = elem || this.element;
			var o = this.options,
				pos = {};
			e = (e && e.type) ? e : {
				type: 'unknown'
			};
			extras = extras || {};
			if (!extras.opener) {
				extras.opener = this.currentOpener;
			}

			if (typeof o.positionType === 'string' && $.ui.cOverlay.posMethods[o.positionType]) {

				pos = $.ui.cOverlay.posMethods[o.positionType](elem, e, extras, this);
			} else if ($.isFunction(o.positionType)) {
				pos = o.positionType(elem, e, extras, this);
			}
			return pos;
		}
	});

	$.ui.cOverlay.posMethods = {};
	$.ui.cOverlay.posMethods.position = function(overlay, e, extra, ui) {
		var o = ui.options,
			target,
			pos;
		if (o.followMouse && e.type.indexOf('mouse') != -1) {
			target = e;
			$(document).bind('mousemove.' + ui.id, function(evt) {
				var delta = {
						top: e.pageY - evt.pageY,
						left: e.pageX - evt.pageX
					},
					posDelta = {
						top: pos.top - delta.top,
						left: pos.left - delta.left
					};
				overlay.css({
					top: pos.top - delta.top,
					left: pos.left - delta.left
				});
			});
		} else if (o.positionOpts.posTarget || extra.opener) {
			target = o.positionOpts.posTarget || ui.currentOpener || extra.opener;
		}
		overlay.position($.extend({}, o.positionOpts, {
			of: target,
			using: function(tmpPos) {
				pos = tmpPos;
			}
		}));
		return pos;
	};
	$.ui.cOverlay.posMethods.around = function(overlay, e, extra, ui) {
		var o = ui.options,
			pos;

		if (!$.posAround) {
			setTimeout(function() {
				throw ('please install the posAround plugin');
			}, 0);
			return {};
		}

		if (o.followMouse && e.type.indexOf('mouse') != -1) {
			pos = $.posAround(overlay, e, o.positionOpts);
			$(document).bind('mousemove.' + ui.id, function(evt) {
				var delta = {
						top: e.pageY - evt.pageY,
						left: e.pageX - evt.pageX
					},
					posDelta = {
						top: pos.top - delta.top,
						left: pos.left - delta.left
					};
				overlay.css({
					top: pos.top - delta.top,
					left: pos.left - delta.left
				});
			});
		} else if (o.positionOpts.posTarget || extra.opener) {
			pos = $.posAround(overlay, o.positionOpts.posTarget || extra.opener, o.positionOpts);
		}
		return pos;
	};

	$.ui.cOverlay.posMethods.centerInsideView = function(overlay, e, extra, ui) {
		var o = ui.options,
			doc = $(document),
			pos;

		if (!$.objScale) {
			setTimeout(function() {
				throw ('please install the objScale plugin');
			}, 0);
			return {};
		}

		pos = $.objScale.centerObjTo(overlay, $(window), o.positionOpts);
		pos.top += doc.scrollTop();
		pos.left += doc.scrollLeft();
		return pos;
	};


	var addFollowScroll = function(overlay, ui) {
		var o = ui.options,
			timer,
			fn;
		if (o.followScroll) {
			fn = overlay.data('followScrollFn');
			if (fn) {
				$(window).unbind('scroll.' + ui.id + ' resize.' + ui.id, fn);
			}
			fn = function(e) {
				var overlayHeight = overlay.outerHeight(true);
				var viewportHeight = $(window).height();
				if (overlayHeight > viewportHeight - 20) {
					return;
				}
				var scrolltop = $.SCROLLROOT.scrollTop();
				var overlayTop = overlay.offset().top;
				clearTimeout(timer);
				if (scrolltop > overlayTop - 10 || scrolltop - 10 < overlayTop + overlayHeight) {
					timer = setTimeout(function() {
						overlay.animate({
							top: $.SCROLLROOT.scrollTop()
						});
					}, 400);

				}
			};
			overlay.data('followScrollFn', fn);
			$(window).bind('scroll.' + ui.id + ' resize.' + ui.id, fn);
		}
	};


	$.ui.cOverlay.posMethods.centerHorizontalView = function(overlay, e, extra, ui) {
		var o = ui.options,
			doc = $(document),
			pos;

		if (!$.objScale) {
			setTimeout(function() {
				throw ('please install the objScale plugin');
			}, 0);
			return {};
		}

		pos = $.objScale.centerObjTo(overlay, $(window), o.positionOpts);
		pos.top = doc.scrollTop();

		if (isFinite(o.marginTop)) {
			pos.top += o.marginTop;
		}

		pos.left += doc.scrollLeft();
		addFollowScroll(overlay, ui);
		return pos;
	};

	$.ui.cOverlay.posMethods.centerHorizontalView.addFollowScroll = addFollowScroll;


})(window.webshims && webshims.$ || jQuery);
