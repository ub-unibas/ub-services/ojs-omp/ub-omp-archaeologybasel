(function($) {
	if (window.jspackager && jspackager.jsPath !== undefined && jspackager.devmode) {
		var curScript = jspackager.jsList["__enhanced-map-modules"];
		if (curScript && curScript.length) {
			if ($ && window.sssl && $(document.scripts || 'script').filter('[src*="__enhanced-map-modules.js"]')[$.fn.prop ? 'prop' : 'attr']('async')) {
				sssl($.map(curScript, function(src) {
					return jspackager.jsPath + src + '.js';
				}));
			} else {
				for (var j = 0, leng = curScript.length; j < leng; j++) {
					document.write('<script src="' + jspackager.jsPath + curScript[j] + '.js"><\/script>');
				}
			}
		}
	}
})(window.jQuery);
if (this.jspackager && !jspackager.devmode) {
	function InfoBox(a) {
		this.defaults = {
			height: 400,
			Width: 300
		}, this.options = $.extend(this.defaults, a);
		var b = this.options;
		google.maps.OverlayView.call(this), this.latlng = b.latlng, this.map = b.map, this.height = b.height, this.width = b.width, this.offsetVertical = -this.height - 70, this.offsetHorizontal = -(this.width / 2) - 7;
		var c = this;
		this.boundsChangedListener = google.maps.event.addListener(this.map, "bounds_changed", function() {
			return c.panMap.apply(c)
		}), this.setMap(this.map)
	}
	"object" == typeof google && "object" == typeof google.maps && (InfoBox.prototype = new google.maps.OverlayView), InfoBox.prototype.remove = function() {
			this.div && (this.div.parentNode.removeChild(this.div), this.div = null, this.setMap(null))
		}, InfoBox.prototype.draw = function() {
			if (this.createElement(), this.div) {
				var a = this.getProjection().fromLatLngToDivPixel(this.latlng);
				this.options, a && (this.div.style.width = this.width + "px", this.div.style.left = a.x + this.offsetHorizontal + "px", this.div.style.height = this.height + "px", this.div.style.top = a.y + this.offsetVertical + "px", this.div.style.display = "block")
			}
		}, InfoBox.prototype.createElement = function() {
			var a = this.getPanes(),
				b = this,
				c = this.options,
				d = this.div;
			if (d) d.parentNode != a.floatPane && (d.parentNode.removeChild(d), a.floatPane.appendChild(d));
			else {
				d = this.div = document.createElement("div"), d.style.position = "absolute", d.style.width = this.width + "px", d.style.height = this.height + "px";
				var e = document.createElement("div");
				e.innerHTML = c.content;
				var f = document.createElement("div"),
					g = e.getElementsByClassName("close");
				$(g).on("click", function() {
					b.remove()
				}), d.appendChild(f), d.appendChild(e), d.style.display = "none", a.floatPane.appendChild(d), this.panMap()
			}
		}, InfoBox.prototype.panMap = function() {
			var a = this.map,
				b = a.getBounds();
			if (b) {
				var c = this.latlng,
					d = this.width,
					e = this.height,
					f = this.offsetHorizontal,
					g = this.offsetVertical,
					h = 40,
					i = 40,
					j = a.getDiv(),
					k = j.offsetWidth,
					l = j.offsetHeight,
					m = b.toSpan(),
					n = m.lng(),
					o = m.lat(),
					p = n / k,
					q = o / l,
					r = b.getSouthWest().lng(),
					s = b.getNorthEast().lng(),
					t = b.getNorthEast().lat(),
					u = b.getSouthWest().lat(),
					v = c.lng() + (f - h) * p,
					w = c.lng() + (f + d + h) * p,
					x = c.lat() - (g - i) * q,
					y = c.lat() - (g + e + i) * q,
					z = (r > v ? r - v : 0) + (w > s ? s - w : 0),
					A = (x > t ? t - x : 0) + (u > y ? u - y : 0),
					B = a.getCenter(),
					C = B.lng() - z,
					D = B.lat() - A;
				a.setCenter(new google.maps.LatLng(D, C)), google.maps.event.removeListener(this.boundsChangedListener), this.boundsChangedListener = null
			}
		},
		function(a) {
			window.HandlerbarsTemplates = function() {
				"undefined" == typeof window.templates && (window.templates = []), a("#locationfinder-template").size() > 0 && (window.templates.locationfinder = Handlebars.compile(a("#locationfinder-template").html())), a("#living-template").size() > 0 && (window.templates.living = Handlebars.compile(a("#living-template").html())), window.templates["living-slides"] = ["{{#each slides}}", '<li  class="carousel-item">', '<div class="carousel-item-inner">', '<img class="normal" src="{{src}}" alt="Bild" title="Bild" />', '<div class="grid-container">', "{{#if title}}", '<section class="info-wrapper">', "<h1>", "{{#if link}}", '<a class="title" href="{{link}}" title="{{linkTitle}}">{{title}}</a>', "{{else}}", '<span class="title">{{title}}</span>', "{{/if}}", "</h1>", "{{#if link}}", '<a href="{{link}}" title="{{linkTitle}}" class="more">' + a.i18n.getText("more") + "</a>", "{{/if}}", "</section>", "{{/if}}", "</div>", "</div>", "</li>", "{{/each}}"].join("\n"), window.templates["lf-list-item"] = ['<a class="marker" id="{{id}}"  data-id="{{id}}" href="#" {{#if url}}data-url="{{url}}"{{/if}} data-long="{{longitude}}" {{#if name}}data-title="{{name}}"{{/if}} {{#if name}}data-name="{{name}}"{{/if}} {{#if content}}data-content="{{content}}"{{/if}} {{#if img}}data-img="{{img}}"{{/if}} data-lat="{{latitude}}">{{#if title}}{{title}}{{/if}}{{#if name}}{{name}}{{/if}}</a>'].join("\n"), window.templates["lf-marker-item"] = ['<span class="marker" id="{{id}}" data-id="{{id}}" href="#" {{#if url}}data-url="{{url}}"{{/if}} data-long="{{longitude}}" {{#if name}}data-title="{{name}}"{{/if}} {{#if name}}data-name="{{name}}"{{/if}} {{#if content}}data-content="{{content}}"{{/if}} {{#if img}}data-img="{{img}}"{{/if}} data-lat="{{latitude}}">{{#if title}}{{title}}{{/if}}{{#if name}}{{name}}{{/if}}</span>'].join("\n"), window.templates["video-iframe"] = ['<iframe width="{{width}}" height="{{height}}" src="{{href}}" allowfullscreen></iframe>'].join("\n"), window.templates.infowindow = ['<div class="infowindow-content desktop">', '<a id="infowindow-close" class="close"></a>', '<img src="{{#infowindowImg img}}{{/infowindowImg}}"/>', "<h1>{{title}}</h1>", "<p>{{content}}</p>", '{{#if url }}<a href="{{url}}" class="button" target="_blank" title="{{title}}">' + a.i18n.getText("visit") + "</a>{{/if}}", "</div>"].join("\n"), window.templates.infowindowNEW = ['<div class="infowindow-content">', '<a id="infowindow-close" class="close"></a>', '<img src="{{#infowindowImg img}}{{/infowindowImg}}"/>', "{{#if title }}<h1>{{title}}</h1>{{/if}}", "{{#if content }}<p>{{content}}</p>{{/if}}", '<a href="#" id="{{id}}" data-id="{{id}}" data-long="{{long}}" data-lat="{{lat}}" class="button open-map" title="{{title}}">Auf Karte anzeigen</a>', '{{#if url }}<a href="{{url}}" class="button" target="_blank" title="{{title}}">' + a.i18n.getText("visit") + "</a>{{/if}}", "</div>"].join("\n"), window.templates["infowindow-mobile"] = ['<div class="infowindow-content mobile">', '<div id="infowindow-close" class="close"></div>', "<h1>{{title}}</h1>", "<p>{{content}}</p>", '{{#if url }}<a href="{{url}}" class="button" target="_blank" title="{{title}}">' + a.i18n.getText("visit") + "</a>{{/if}}", "</div>"].join("\n"), window.templates.twitter = ['<div class="media_box">', '<div class="socialcontent twitter">', '<abbr class="timeago" title="{{#toISOString data.createdAt}}{{/toISOString}}"></abbr>', "<h4>", '<a title="' + a.i18n.getText("openNewTab") + '" href="{{data.user.profileUrl}}/status/{{id}}" target ="_blank">{{data.text}}</a>', "</h4>", "</div>", '<footer class="simple_box_footer">', '<a target="_blank" href="{{data.user.profileUrl}}" title="' + a.i18n.getText("twitter") + '" class="button wide">' + a.i18n.getText("twitter") + "</a>", "</footer>", "</div>"].join("\n"), window.templates.facebook = ['<div class="media_box">', '<div class="image_wrapper">', '<a title="' + a.i18n.getText("openNewTab") + '" target="_blank" href="{{data.link}}">', '<img class="socialimg" src="{{data.picture}}" alt="{{data.message}}" />', "</a>", "</div>", '<div class="socialcontent facebook">', '<abbr class="timeago" title="{{#toISOString data.createdTime}}{{/toISOString}}"></abbr>', "<h4>", '<a title="' + a.i18n.getText("openNewTab") + '" target="_blank" href="{{data.link}}">{{data.message}}</a>', "</h4>", "</div>", '<footer class="simple_box_footer">', '<a target="_blank" href="https://www.facebook.com/{{data.from.id}}" title="' + a.i18n.getText("facebook") + '" class="button wide">' + a.i18n.getText("facebook") + "</a>", "</footer>", "</div>"].join("\n"), window.templates.youtube = ['<div class="media_box">', '<div class="image_wrapper">', '<a title="' + a.i18n.getText("openNewTab") + '" href="{{data.embeddedWebPlayerUrl}}?autoplay=1" target="_blank">', '<img src="{{#thumbnails data.thumbnails}}{{/thumbnails}}" alt="{{data.title}}"/>', "</a>", "</div>", '<div class="socialcontent youtube">', '<abbr class="timeago" title="{{#toISOString data.createdTime}}{{/toISOString}}"></abbr>', '<h4><a title="' + a.i18n.getText("openNewTab") + '" href="{{data.embeddedWebPlayerUrl}}?autoplay=1" target="_blank">{{data.title}}</h4>', "</div>", '<footer class="simple_box_footer">', '<a target="_blank" href="{{data.channel}}" title="' + a.i18n.getText("youtube") + '" class="button wide">' + a.i18n.getText("youtube") + "</a>", "</footer>", "</div>"].join("\n"), window.templates.instagram = ['<div class="media_box">', '<div class="image_wrapper">', '<a title="' + a.i18n.getText("openNewTab") + '" href="{{data.link}}" target="_blank">', '<img src="{{#thumbnails data.images.thumbnail}}{{/thumbnails}}" alt="{{data.caption.text}}" />', "</a>", "</div>", '<div class="socialcontent instagram">', '<abbr class="timeago" title="{{#toISOString data.caption.created_time}}{{/toISOString}}"></abbr>', '<h4><a title="' + a.i18n.getText("openNewTab") + '" href="{{data.link}}" target="_blank">{{data.caption.text}}</a></h4>', "</div>", '<footer class="simple_box_footer">', '<a target="_blank" href="{{profile.url}}" title="' + a.i18n.getText("instagram") + '" class="button wide">' + a.i18n.getText("instagram") + "</a>", "</footer>", "</div>"].join("\n"), window.templates.documents = ["{{#each docs}}", '<section class="document_item">', '<h1><a class="download" href="{{link}}" title="{{fileExtension}} öffnet in neuem Fenster" target="_blank">{{fileName}} ({{fileExtension}}, {{fileSize}})</a></h1>', "<em>{{{lastModified}}}</em>", "<p>{{{description}}}</p>", "</section>", "{{/each}}"].join("\n")
			}, Handlebars.registerHelper("toISOString", function(a) {
				var b = new Date(a);
				return b.toISOString()
			}), Handlebars.registerHelper("thumbnails", function(a) {
				return a[0] ? a[0].imageUrl : a.url
			}), Handlebars.registerHelper("infowindowImg", function(a) {
				var b = window.location.href.indexOf("devmode") >= 0 || !window.magnoliaFrontendData ? "img/gmaps/placeholder.png" : window.magnoliaFrontendData.themePath + "/img/gmaps/placeholder.png";
				return a ? a : b
			})
		}(window.webshims && webshims.$ || jQuery),
		function(a) {
			"use strict";
			var b = "on";
			a.widget("aperto.carousel", {
				options: {
					selectedIndex: 0,
					type: "normal",
					hasThumbView: !1,
					isThumbView: !1,
					pagination: !0,
					paginationGrouping: !1,
					itemAdjust: !0,
					viewportBreaks: {
						800: 1,
						1270: 1,
						1600: 1,
						1900: 1
					},
					paginationAtoms: '<li class="pagination-item pa-$number" data-pa-index="$index"><button class="pagination-item-button"><span>$number</span></button></li>',
					autoplay: !1,
					nextBtn: ".stage-next",
					prevBtn: ".stage-prev",
					paginationSel: ".pagination",
					view: ".carousel-wrapper",
					doc: ".carousel-area",
					itemSel: ".carousel-item > .carousel-item-inner"
				},
				_create: function() {
					var c = this;
					if (this.$view = this.element.find(this.options.view), this.$doc = this.$view.find(this.options.doc), this.viewBox = {}, this.isAnimated = this.$doc.is(".animate"), this.hasThumbView = this.options.hasThumbView, this.isThumbView = this.options.isThumbView, this.updateViewBox(), this.addResize(), this.itemAdjust(!0), this.addTouchControls(), this.isThumbView ? this.thumbnailPagination(!0) : this.pagination(!0), this.addStepButtons(), this._created = !0, "endless" === this.options.type && this.updateDisabledState(), this.hasThumbView) {
						var d = this.element.attr("rel");
						this.thumbView = a("#" + d), this.thumbView.find(this.options.itemSel).on("click", function(b) {
							b.preventDefault();
							var d = a(this).parent().index();
							c.selectedIndex(d), c.updatePagination(), a(window).trigger("thumbnailclicked", d)
						})
					}
					this.isThumbView && (a(c.$doc.find(c.options.itemSel)[0]).addClass(b), a(window).on("thumbnailclicked", function(d, e) {
						var f = c.$doc.find(c.options.itemSel);
						f.removeClass(b), a(f[e]).addClass(b)
					})), this.isThumbView ? (a(this.options.prevBtn, this.element).remove(), a(this.options.nextBtn, this.element).remove()) : (a(this.options.prevBtn, this.element).addClass("enabled").attr("tabindex", "-1"), a(this.options.nextBtn, this.element).addClass("enabled").attr("tabindex", "-1")), this.options.autoplay && this.initAutoplay()
				},
				pagination: function(c) {
					if (c) {
						var d = this.options,
							e = this;
						this.pagination = a(this.options.paginationSel, this.element);
						var f = '<ul class="pagination-item-list">';
						this.$doc.children().each(function(a) {
							f += d.paginationAtoms.replace(/\$number/g, a + 1).replace(/\$index/g, a)
						}), this.pagination.html(f + "</ul>"), this.paginationBtns = a("li", this.pagination), this.paginationBtns.eq(this.selectedIndex()).addClass(b), setTimeout(function() {
							e.updatePagination()
						}), d.paginationGrouping && a(window).on("resize", a.Aperto.throttle(function() {
							e.updatePagination()
						})), this.paginationBtns.touchClick(function() {
							var b = a(this),
								c = e.selectedIndex(),
								d = e.paginationBtns.index(b);
							d !== c && (e.selectedIndex(d), e.updatePagination(), e.hasThumbView && e.updateThumbview(d))
						}), this.$doc.children().length <= 1 && this.pagination.addClass("disabled")
					}
				},
				updateThumbview: function(c) {
					var d = this.$doc.find(".carousel-item").length,
						e = this.thumbView.find(".pagination-item").length;
					this.thumbView.find(".carousel-item-inner").removeClass(b), a(this.thumbView.find(".carousel-item-inner")[c]).addClass(b);
					var f = Math.ceil((c + 1) / (d / e));
					a(this.thumbView.find(".pagination-item")[f - 1]).trigger("click")
				},
				thumbnailPagination: function(c) {
					if (c) {
						var d = this.options,
							e = this;
						this.pagination = a(this.options.paginationSel, this.element);
						var f = '<ul class="pagination-item-list">';
						e.slidesCount = this.$doc.children().length / this.options.viewportBreaks["800"];
						for (var g = 0; g < e.slidesCount; g++) f += d.paginationAtoms.replace(/\$number/g, g + 1).replace(/\$index/g, g);
						this.pagination.html(f + "</ul>"), this.paginationBtns = a("li", this.pagination), e.paginationBtns.removeClass(b), a(e.paginationBtns[0]).addClass(b), this.paginationBtns.touchClick(function() {
							var c = a(this),
								d = 4 * c.index();
							e.paginationBtns.removeClass(b), c.addClass(b), e.selectedIndex(d)
						})
					}
				},
				updatePagination: function() {
					var a = this,
						c = this.options.paginationGrouping ? a.getSelectedIndexes() : [a.selectedIndex(), a.selectedIndex()];
					a.paginationBtns.removeClass(b), a.paginationBtns.slice(c[0], c[1] + 1).addClass(b)
				},
				indexFromLeft: function(b) {
					b || (b = 0);
					var c = 0,
						d = -1 * (this.position() + b);
					return this.$doc.children().each(function(b) {
						var e = a(this).position();
						return e ? e.left + 1 >= d ? (c = b, !1) : void 0 : (this.error("something happened"), c = b, !1)
					}), c
				},
				indexFromRight: function(b) {
					b || (b = 0);
					for (var c, d = -1 * (this.position() + b), e = this.$doc.children(), f = e.length; f-- && f;) {
						if (c = a(e[f]).position(), !c) {
							this.error("something happened");
							break
						}
						if (c.left - 1 <= d) break
					}
					return f
				},
				snapTo: function(a) {
					var b;
					b = 0 > a ? this.indexFromLeft() : this.indexFromRight(), this.selectedIndex(b), this.updatePagination()
				},
				setRelPos: function(a) {
					this.setAnimate(!1), this.position(this.position() + a)
				},
				setAnimate: function(a) {
					a != this.isAnimated && (this.isAnimated = a, this.$doc[a ? "addClass" : "removeClass"]("animate"))
				},
				updateViewBox: function() {
					this.adjustItems(), this.viewBox.min = 0, this.viewBox.width = this.$view.width();
					var a = this.$doc.outerWidth(!0) - this.viewBox.width;
					a !== this.viewBox.max && (this.viewBox.max = a, this.selectedIndex(this.options.selectedIndex, !0))
				},
				selectNext: function() {
					var a = this.selectedIndex(),
						b = this.$doc.children().length,
						c = a + 1;
					a >= b - 1 && (c = 0), this.selectedIndex(c)
				},
				selectPrev: function() {
					var a = this.selectedIndex(),
						b = this.$doc.children().length,
						c = a - 1;
					0 >= a && (c = b - 1), this.selectedIndex(c)
				},
				getSelectedIndexes: function() {
					var a, b, c;
					if (!this._selectedIndexes)
						if (b = Math.abs(this.position()), a = this.$doc.children(), this._selectedIndexes = [this.options.selectedIndex, this.options.selectedIndex], this.reachedEnd())
							for (this._selectedIndexes[1] = [a.length - 1], c = a.length - 1; c >= 0 && (this._selectedIndexes[0] = c, !(a.get(c).offsetLeft - c - 5 <= b)); c--);
						else
							for (b += this.viewBox.width, c = this.options.selectedIndex; c < a.length && !(a.get(c).offsetLeft + c + 5 >= b); c++) this._selectedIndexes[1] = c;
					return this._selectedIndexes
				},
				selectedIndex: function(a, b) {
					var c, d;
					if (!arguments.length) return this.options.selectedIndex;
					if (d = this.$doc.children(), !(0 > a || d.length <= a || !this._created && !a && this.options.selectedIndex == a)) return (c = d.eq(a).position()) ? (c = -1 * Math.min(Math.max(c.left, this.viewBox.min), this.viewBox.max), this.setAnimate(this._created && !b), this.position(c), this._selectedIndexes = null, "normal" == this.options.type && this.updateStartEnd(), this.options.selectedIndex != a && (this.options.selectedIndex = a, this.element.trigger("indexchange")), void 0) : (this.error("something bad happened"), void 0)
				},
				position: function(a) {
					return arguments.length ? (this.$doc[0].style.left = a + "px", void 0) : parseFloat(this.$doc.css("left"), 10) || 0
				},
				updateStartEnd: function() {
					var a = -1 * this.position() < 1,
						b = -1 * this.position() > this.viewBox.max - 1;
					(a !== this._atStart || b !== this._atEnd) && (this._atStart = a, this._atEnd = b, "endless" === this.options.type && this.updateDisabledState())
				},
				reachedStart: function() {
					return this._atStart
				},
				reachedEnd: function() {
					return this._atEnd
				},
				addStepButtons: function() {
					var a = this;
					this.element.touchClick(this.options.nextBtn, function() {
						a.selectNext(), a.updatePagination(), a.hasThumbView && a.updateThumbview(a.selectedIndex())
					}), this.element.touchClick(this.options.prevBtn, function() {
						a.selectPrev(), a.updatePagination(), a.hasThumbView && a.updateThumbview(a.selectedIndex())
					}), "endless" === this.options.type && this.updateDisabledState()
				},
				updateDisabledState: function() {
					a(this.options.prevBtn, this.element).prop("disabled", this.reachedStart()), a(this.options.nextBtn, this.element).prop("disabled", this.reachedEnd())
				},
				addResize: function() {
					var b = this;
					a(window).on("resize", a.Aperto.throttle(function() {
						b.updateViewBox()
					}))
				},
				addTouchControls: function() {
					var a, b, c, d, e = this,
						f = this,
						g = this.$doc,
						h = !1,
						i = function() {
							h = !1
						},
						j = function() {
							h = !0, clearTimeout(d), d = setTimeout(i, 301)
						},
						k = function(c) {
							var d = c.originalEvent,
								e = d.changedTouches || d.touches;
							1 == e.length && Math.abs(b - a) > 15 && (j(), c.preventDefault(), f.snapTo(b - a), m())
						},
						l = function(a) {
							var d = a;
							a = a.originalEvent;
							var e = (a.changedTouches || a.touches)[0],
								g = e.pageX,
								h = e.pageY,
								i = b - g,
								j = c - h;
							b = g, c = h, Math.abs(i) && (Math.abs(j) || d.preventDefault(), f.setRelPos(-1 * i))
						},
						m = function() {
							g.off("touchmove", l).off("touchend", k)
						};
					g.on("click", function(a) {
						(h || e.isThumbView) && (a.preventDefault(), a.stopImmediatePropagation())
					}).on("touchstart", function(d) {
						d = d.originalEvent, 1 == d.touches.length && (m(), a = d.touches[0].pageX, b = d.touches[0].pageX, c = d.touches[0].pageY, g.on("touchmove", l), g.on("touchend", k))
					})
				},
				itemAdjust: function(a) {
					arguments.length && a !== !1 && (this.adjustItems(), this.updateViewBox())
				},
				adjustItems: function() {
					var b, c = 1,
						d = this.$view.innerWidth(),
						e = a(this.options.itemSel, this.$view);
					a.each(this.options.viewportBreaks, function(a, b) {
						return c = b, a >= d ? !1 : void 0
					}), c ? (b = d / c - (parseFloat(a(e[1]).css("marginLeft"), 10) || 0) - (parseFloat(a(e[1]).css("marginRight"), 10) || 0), e.outerWidth(b)) : e.css({
						width: ""
					})
				},
				initAutoplay: function() {
					var b = this;
					b.playPauseBtn = a('<span tabindex="0" role="button" class="play-pause-btn">' + a.i18n.getText("isPlaying") + "</span>").appendTo(this.$view), b.playPauseBtn.addClass("isPlaying");
					var c = 8e3;
					b.playPauseBtn.on("click", function(d) {
						d.preventDefault(), a(this).is(".isPlaying") ? (a(this).removeClass("isPlaying").addClass("isPausing").text(a.i18n.getText("isPausing")), window.clearInterval(b.myInterval)) : (a(this).removeClass("isPausing").addClass("isPlaying").text(a.i18n.getText("isPlaying")), b.myInterval = window.setInterval(function() {
							b.selectNext(), b.updatePagination()
						}, c))
					}), b.myInterval = window.setInterval(function() {
						b.selectNext(), b.updatePagination()
					}, c)
				}
			})
		}(window.webshim && webshim.$ || jQuery),
		function(a) {
			"undefined" == typeof window.aperto && (window.aperto = {});
			var b, c, d = 200;
			String.prototype.hashCode = function() {
				var a, b, c, d = 0;
				if (0 == this.length) return d;
				for (a = 0, c = this.length; c > a; a++) b = this.charCodeAt(a), d = (d << 5) - d + b, d |= 0;
				return d
			}, Array.prototype.containsJQuery = function(a) {
				for (var b = this.length - 1; b > -1;) {
					if (this[b][0] === a[0]) return !0;
					b--
				}
			}, Array.prototype.removeJQuery = function(a) {
				for (var b = this.length - 1; b > -1;) {
					if (this[b][0] === a[0]) return this.slice(b, 1);
					b--
				}
			}, aperto.ContainerToggle = function(b) {
				this.defaults = {
					isDebug: !1,
					animations: [],
					moderator: this,
					animation: "slide",
					onShow: function(a, b, c) {
						b.animations[c.animation], "slide" === c.animation ? b.$target.slideDown(c.speed, function() {
							a && a(), b.$target.trigger("dommodified"), b.$target.trigger("updatedom"), b.$target.trigger("equalRowHeight:initialize", [{
								target: b.$target
							}]), b.isDebug && console.log("_showAnimation complete"), b.$el.trigger("containerToggle:complete", [{
								ui: b
							}]), "click" === c.eventType && c.scrollViewportTillExpandable && b._scrollViewportTillExpandable(), c.autoHeight && b.$target.css("height", "auto"), b._toggleHandle("hide")
						}) : b.$target.fadeIn(c.speed, function() {
							a && a(), b.$target.trigger("dommodified"), b.$target.trigger("updatedom"), b.$target.trigger("equalRowHeight:initialize", [{
								target: b.$target
							}]), b.isDebug && console.log("_showAnimation complete"), b.$el.trigger("containerToggle:complete", [{
								ui: b
							}]), "click" === c.eventType && c.scrollViewportTillExpandable && b._scrollViewportTillExpandable(), c.autoHeight && b.$target.css("height", "auto"), b._toggleHandle("hide")
						})
					},
					onHide: function(a, b, c) {
						b.animations[c.animation], "slide" === c.animation ? b.$target.slideUp(c.speed, function() {
							a && a(), b.isDebug && console.log("_hideAnimation complete"), c.autoHeight && b.$target.css("height", "auto"), b._toggleHandle("show")
						}) : b.$target.fadeOut(c.speed, function() {
							a && a(), b.isDebug && console.log("_hideAnimation complete"), c.autoHeight && b.$target.css("height", "auto"), b._toggleHandle("show")
						})
					},
					context: document,
					handle: ".toggle",
					expandable: ".expandable",
					activeClass: "active",
					disableClass: "off",
					subnavClass: "on",
					beforeSubnavClass: "before-subnav",
					speed: 0,
					scrollTop: !1,
					enableAria: !1,
					focus: !0,
					scrollViewportTillExpandable: !1,
					desktopWith: 1024,
					mobileWidth: 768,
					noBorderClass: void 0,
					activateSmartFocus: !0,
					closeOnFocusout: !1,
					autoHeight: !1,
					tabMode: void 0,
					initialTab: void 0,
					initialTabClass: "open",
					initialContent: ".initial-content",
					toggleContent: !1,
					equalHeight: !1,
					toggleHandle: void 0,
					toggleVisibilityToggleHandle: !0,
					deviceAdaptedTargets: !1,
					rejectedDevices: [],
					start: function() {},
					before: function() {},
					after: function() {},
					complete: function() {},
					resize: function() {}
				}, this.options = a.extend(this.defaults, b), this.$el = a(this.options.context, this.element), this.$widgetSelector = a(this.options.selector, this.$el), this.isDebug = this.options.isDebug, this._initWidget()
			}, aperto.ContainerToggle.prototype = {
				_initWidget: function() {
					var b = this,
						c = this.options;
					this.$el.attr("data-" + c.selector.hashCode()) || (this.$el.attr("data-" + c.selector.hashCode(), !0), this.activeEls = [], this.viewportWidth = window.innerWidth, this._getDeviceMode(c), this._defineAnimations(c), this.$widgetSelector.containerToggle(this.options), this._bindEvents(this, c), a.webshims.ready("WINDOWLOAD", function() {
						b._equalDivsHeight(c), c.after(), b._openInitialTab(c)
					}), a(window).on("emchange", function() {
						b._equalDivsHeight(c)
					}))
				},
				_bindEvents: function(b, c) {
					this._changeMouseEvents(c), a(window).smartresize(function(a) {
						b.viewportWidth !== window.innerWidth && (b.viewportWidth = window.innerWidth, b._changeMouseEvents(c, a), b._deviceAdaptedTargets(c, a), b.sizeUpdate())
					})
				},
				_defineAnimations: function(a) {
					a.animations.slide = {
						show: "slideDown",
						hide: "slideUp"
					}, a.animations.fade = {
						show: "fadeIn",
						hide: "fadeOut"
					}
				},
				_changeMouseEvents: function(a) {
					this._getDeviceMode(a);
					var b = this.$widgetSelector.data("aperto-containerToggle"),
						c = "desktop" === this.mode,
						d = a.rejectedDevices.indexOf(this.mode) > -1;
					if (b) switch (!0) {
						case d:
							this.$widgetSelector.containerToggle("disableEl", void 0, !0), this.$widgetSelector.containerToggle("unbindMouseEvents");
							break;
						case a.enableHover && c:
							this.$widgetSelector.containerToggle("bindMouseEvents", "hover");
							break;
						default:
							this.$widgetSelector.containerToggle("bindMouseEvents", "click")
					}
				},
				_deviceAdaptedTargets: function(a) {
					this._getDeviceMode(a), a.deviceAdaptedTargets && this.$widgetSelector.containerToggle("getTarget", "hover")
				},
				activeHandle: function(a) {
					this.$widgetSelector.containerToggle("showHandleWithSameTarget", a)
				},
				disableHandle: function(a) {
					this.$widgetSelector.containerToggle("hideHandleWithSameTarget", a)
				},
				disableElement: function(a) {
					for (this.activeEls.length, this.resetLinkBorders(this.options); this.activeEls.length;) this.isDebug && console.log("Array: ", this.activeEls), this.activeEls.pop().containerToggle("disableEl", void 0, a)
				},
				sizeUpdate: function() {
					var a = this.options;
					this._equalDivsHeight(a), a.resize(), this._openInitialTab(a)
				},
				_openInitialTab: function(b) {
					var c = "undefined" != typeof b.initialTab ? b.initialTab : this.$widgetSelector.parent().closest("." + b.initialTabClass).index();
					return "undefined" != typeof c && c > -1 ? (this.$widgetSelector.eq(c).containerToggle("enableEl"), void 0) : (this.$widgetSelector.hasClass(b.initialTabClass) && this.$widgetSelector.each(function(c, d) {
						var e = a(d);
						return e.hasClass(b.initialTabClass) ? (e.containerToggle("enableEl"), void 0) : void 0
					}), void 0)
				},
				_equalDivsHeight: function(b) {
					b.equalHeight && (b.toggleContent && this.$widgetSelector.containerToggle("disableEl"), a(b.expandable + ", " + b.initialContent, this.$el).css("height", "auto"), this.$widgetSelector.containerToggle("toggleVisibility", !1), this._setMaxHeight(b), this.$widgetSelector.containerToggle("toggleVisibility", !0))
				},
				_setMaxHeight: function(b) {
					var c = a(b.expandable + ", " + b.initialContent, this.$el),
						d = 0;
					c.each(function(b, c) {
						var e = a(c).height();
						e > d && (d = e)
					}), this.isDebug && console.log("Resulting Height => ", d), c.height(d), this.$el.trigger({
						type: "containerToggle:height",
						elements: c
					})
				},
				_getDeviceMode: function(a) {
					var b = window.innerWidth;
					switch (this.previousMode = this.mode, !0) {
						case b <= a.mobileWidth:
							this.mode = "mobile";
							break;
						case b > a.mobileWidth && b < a.desktopWith:
							this.mode = "tablet";
							break;
						default:
							this.mode = "desktop"
					}
					this.isDebug && console.log("Device: ", this.mode)
				},
				removeBorderPreviousLink: function(a, b, c) {
					if (b.noBorderClass) {
						var d = c ? b.noBorderClass + " before-subnav" : b.noBorderClass;
						0 != a && (this.$noBorderItem = this.$widgetSelector.eq(a - 1).addClass(d))
					}
				},
				resetLinkBorders: function(a) {
					this.$noBorderItem && !this.$noBorderItem.hasClass(a.beforeSubnavClass) && this.$noBorderItem.removeClass(a.noBorderClass)
				}
			}, a.widget("aperto.containerToggle", {
				options: {},
				_create: function() {
					var b = this.options;
					this.$el = a(this.element[0]), this.moderator = b.moderator, this.animations = b.animations, this.getTarget(), this.$focusable = a("a, input", this.$target), this.$toggleHandle = "undefined" != typeof b.toggleHandle ? this.$target.siblings(b.toggleHandle).size() > 0 ? this.$target.siblings(b.toggleHandle) : a(b.toggleHandle, this.$target) : void 0, this.$initialContent = this.$target.siblings(b.initialContent), this.isStarted = !1, this.isActive = !1, this.disable = !1;
					var c = b.subnavHandle;
					this.isSubnavHandle = c && c.apply(this).hasClass(b.subnavClass), this.isDebug = b.isDebug, this._subnavigationRole(b), this._bindEvents()
				},
				_bindEvents: function() {
					var b = this;
					this.$el.on("containerToggle:open", this.enableEl.bind(this)), a(document).on("containerToggle:targetBlur", this._targetBlur.bind(this)), "undefined" != typeof this.$toggleHandle && (this.isDebug && console.log("ToggleHandle bind event"), this.$toggleHandle.on("click", this.disableEl.bind(this))), this.$target.on("activateWidget", function(a) {
						b.activateWidget(a)
					}), this.$target.on("deactivateWidget", function(a) {
						b.deactivateWidget(a)
					}), this.options.activateSmartFocus && this.$el.on("focus", this._tabFocus.bind(this))
				},
				bindMouseEvents: function(e) {
					function f(a) {
						(!j.isActive || k.tabMode) && (b = setTimeout(function() {
							j._onClickHandle(a)
						}, d))
					}

					function g() {
						clearTimeout(b), c = setTimeout(function() {
							j.moderator.disableElement(this), j.moderator.resetLinkBorders(k)
						}, d)
					}

					function h() {
						clearTimeout(c)
					}

					function i(b) {
						j.isDebug && console.log("event.target: ", b.target, ", event.currentTarget: ", b.currentTarget, ", event.relatedTarget: ", b.relatedTarget), a(b.currentTarget).find(b.target).size() > 0 || (j.moderator.disableElement(this), j.moderator.resetLinkBorders(k))
					}
					var j = this,
						k = this.options,
						l = this.$target.parent();
					this.unbindMouseEvents(l), "click" === e || this.isSubnavHandle ? (this.$el.on("click", function(a) {
						j._onClickHandle(a)
					}), this.$el.off("mouseover"), this.$el.off("mouseout"), l && (l.off("mouseover"), l.off("mouseout"))) : (this.$el.off("click"), this.$el.on("mouseover", f), this.$el.on("mouseout", g), l.on("mouseover", h), l.on("mouseout", i))
				},
				unbindMouseEvents: function(a) {
					this.$el.off("click"), this.$el.off("mouseover"), this.$el.off("mouseout"), a && (a.off("mouseover"), a.off("mouseout"))
				},
				_tabFocus: function() {
					var b = this;
					this.$el.on("keydown", function(c) {
						var d = function() {
							b.enableEl(void 0, function() {
								b.$focusable = a(":focusable", b.$target).filter(":visible"), a(b.$focusable).filter("[tabindex='0']").size() > 0 || (c.shiftKey && 13 !== c.which ? b.$focusable.last().focus() : b.$focusable.first().focus(), this.isDebug && console.log("Tab focus index => ", b.$focusable.index(document.activeElement), ", => ", document.activeElement), b._focusTargetArea())
							})
						};
						9 === c.which && (b.options.enableHover || b.isActive) && (c.preventDefault(), d()), 13 !== c.which || b.isActive || (c.preventDefault(), d())
					})
				},
				_targetBlur: function(a) {
					a.currentFocusedTarget[0] !== this.$target[0] && this.$target.off("keydown")
				},
				_focusTargetArea: function() {
					var b, c = this,
						d = this.$focusable.size(),
						e = this.moderator.$widgetSelector.index(this.$el);
					this.$el.off("keydown"), a(document).trigger({
						type: "containerToggle:targetBlur",
						currentFocusedTarget: this.$target
					}), this.$target.off("keydown"), this.$target.on("keydown", function(f) {
						if (9 === f.which)
							if (f.preventDefault(), b = c.$focusable.index(document.activeElement), f.shiftKey)
								if (0 > b - 1) {
									var g, h = e - 1 > -1 ? c.moderator.$widgetSelector.get(e - 1) : void 0;
									h ? h.focus() : g = function() {
										var b = a(":focusable").index(c.$el);
										b > -1 && (a(":focusable").eq(b - 1).focus(), c.$target.off("keydown"))
									}, c.options.closeOnFocusout && c.disableEl(void 0, void 0, g), a(this).off("keydown")
								} else a(c.$focusable[b - 1]).focus();
						else if (b + 1 >= d) {
							var i, j = c.moderator.$widgetSelector.get(e + 1);
							j ? j.focus() : i = function() {
								var b = a(":focusable").index(c.$el);
								b > -1 && (a(":focusable").eq(b + 1).focus(), c.$target.off("keydown"))
							}, c.options.closeOnFocusout && c.disableEl(void 0, void 0, i), a(this).off("keydown")
						} else a(c.$focusable[b + 1]).focus()
					})
				},
				_subnavigationRole: function(b) {
					this.isSubnavHandle && (this.$target = a(b.subnavEl), b.activeClass = b.subnavClass, this.moderator.removeBorderPreviousLink(this.$el.parent().index(), b, !0), this.activateWidget())
				},
				getTarget: function() {
					var b = this.$el.data("target"),
						c = "undefined" != typeof b && b.length > 0 ? a("#" + b) : this.$el.closest(this.options.expandable),
						c = c.size() < 1 ? this.$el.siblings(this.options.expandable) : c,
						c = c.size() < 1 ? a(this.options.expandable, this.$el) : c;
					this.isDebug && console.log("Data target:", c), this.$target = this.options.deviceAdaptedTargets ? c.not("." + this.moderator.mode) : c, this.options.deviceAdaptedTargets && (c.filter("." + this.moderator.mode).show(), this.disableEl())
				},
				_isStarted: function() {
					this.isStarted || (this.isDebug && console.log("containerToggle:start", this), this.$el.trigger("containerToggle:start", [{
						ui: this
					}]), this.isStarted = !0)
				},
				_onClickHandle: function(a) {
					a.preventDefault(), this.disable || (this.isDebug && console.log("Click event", a), this.isActive && !this.options.tabMode ? this.disableEl(a) : this.enableEl(a), this._scrollToTop())
				},
				enableEl: function(b, c) {
					this.isDebug && console.log("is inactive", this.$el);
					var d = this.options;
					this._isStarted(), this.isSubnavHandle || this.moderator.disableElement(this), this.moderator.activeHandle(this), this.moderator.removeBorderPreviousLink(this.$el.parent().index(), d), this.activateWidget(), this.$target.trigger({
						type: "activateWidget",
						moderator: this.moderator
					});
					try {
						b && (d.eventType = b.type), d.onShow(c, this, d)
					} catch (e) {
						console.log("Animation not available ", e)
					}
					this.moderator.$widgetSelector.each(function() {
						a(a(this).parent()).is(".on") && a("#sublevel").hide()
					}), this.$el.trigger("containerToggle:change")
				},
				disableEl: function(a, b, c) {
					if (this.isDebug && console.log("disableEl ", a, ", children: ", b), a && a.preventDefault(), this.isActive || "undefined" == typeof b || !this.isSubnavHandle || b.isSubnavHandle) {
						var d = this.options;
						this.deactivateWidget(), this.$target.trigger({
							type: "deactivateWidget",
							moderator: this.moderator
						});
						try {
							d.onHide(c, this, d)
						} catch (e) {
							console.log("Animation not available ", e)
						}
					} else this.$target.show()
				},
				_scrollViewportTillExpandable: function() {
					this.isDebug && console.log("_scrollViewportTillExpandable: ", this.$target);
					var b = this.$target.closest("li.item").size() > 0 ? this.$target.closest("li.item") : this.$target.parent();
					a("body, html").animate({
						scrollTop: b.offset().top
					}, 100)
				},
				_toggleHandle: function(a) {
					"undefined" != typeof this.$toggleHandle && ("hide" === a ? (this.options.toggleVisibilityToggleHandle && this.$el.hide(), this.$toggleHandle.addClass(this.options.activeClass), this.unbindMouseEvents()) : (this.options.toggleVisibilityToggleHandle && this.$el.show(), this.$toggleHandle.removeClass(this.options.activeClass), this.bindMouseEvents("click")))
				},
				showHandleWithSameTarget: function(a) {
					this.isDebug && console.log("containerToggle:activeHandle", event), this.$target[0] === a.$target[0] && (this.$el.addClass(this.options.activeClass), this.$target.trigger({
						type: "activateWidget",
						moderator: this.moderator
					}))
				},
				hideHandleWithSameTarget: function(a) {
					this.isDebug && console.log("containerToggle:disableHandle", event), this.$target[0] === a.$target[0] && (this.$el.removeClass(this.options.activeClass), this.$target.trigger({
						type: "deactivateWidget",
						moderator: this.moderator
					}))
				},
				enableHandle: function() {
					this.$el.removeClass(this.options.disableClass)
				},
				disableHandle: function() {
					this.$el.addClass(this.options.disableClass)
				},
				_scrollToTop: function() {
					this.options.scrollTop && (a("body").scrollTop(0), a(document).scrollTop(0))
				},
				activateWidget: function(a) {
					this.isDebug && console.log("activateWidget", a), this.isActive = !0, this.moderator.activeEls.containsJQuery(this.$el) || this.moderator.activeEls.push(this.$el), this.$el.addClass(this.options.activeClass)
				},
				deactivateWidget: function(a) {
					this.isDebug && console.log("deactivateWidget", a), this.isActive = !1, this.moderator.activeEls.removeJQuery(this.$el), this.$el.removeClass(this.options.activeClass)
				},
				disableWidget: function() {
					this.disable = !0
				},
				enableWidget: function() {
					this.disable = !1
				},
				toggleVisibility: function(a) {
					this.$target.css({
						visibility: a ? "visible" : "hidden",
						display: a ? "none" : "block"
					})
				}
			})
		}(window.webshims && webshims.$ || jQuery),
		function(a) {
			"use strict";
			a.widget("aperto.carouselMapModule", {
				options: {
					template: "living",
					infowindowTemplate: "infowindow",
					infowindowMobileTemplate: "infowindow-mobile",
					slidesTemplate: "living-slides",
					tabSelector: "#tab-selector",
					toggleSelector: ".carousel-map-module",
					carouselWrapperId: "carousel-container",
					mapWrapperId: "map-module-wrapper",
					mapCanvasId: "map-module-canvas",
					loader: ".loading",
					isDebug: !1,
					initialTab: 0,
					initialMode: 0,
					monoMode: !1,
					mobileWidth: 767,
					mobileAspectRatio: .375,
					desktopAspectRatio: .328125,
					zoomIn: ".zoom-control .zoom-in",
					zoomOut: ".zoom-control .zoom-out"
				},
				templates: {
					slidesList: '<div class="carousel"><div class="carousel-wrapper"><ul class="carousel-area"></ul><div class="pagination"></div></div><button class="stage-prev" type="button">prev</button><button class="stage-next" type="button">next</button></div>'
				},
				_create: function() {
					var a = this.options;
					this.template = window.templates[a.template], this.slidesTemplate = Handlebars.compile(window.templates[a.slidesTemplate]), this.infowindowTemplate = Handlebars.compile(window.templates[a.infowindowTemplate]), this.infowindowMobileTemplate = Handlebars.compile(window.templates[a.infowindowMobileTemplate]), this.dataMakers = [], this.dataBounds = [], this.selector = a.initialTab, this.isDebug = a.isDebug, this._initialize(), this._getData()
				},
				_initialize: function(b) {
					var c = this.options;
					b && "resize" == b.type || b && this.viewportWidth === window.innerWidth || (this.$el = a(this.element), this.$el[0].innerHTML = this.template(), this.$loader = a(c.loader, this.$el), this._toggleLoader("on"), this.$carouselWrapper = a("#" + c.carouselWrapperId, this.$el), this.$mapWrapper = a("#" + c.mapWrapperId, this.$el), this.$tabSelectors = a(c.tabSelector, this.$el), this.$toggleSelectors = a(c.toggleSelector, this.$el), this.mapRendered = !1, this._getDeviceMode(c), this._bindEvents(this, c), "undefined" == typeof google && (c.monoMode = !0), !c.monoMode && this.mode && new aperto.ContainerToggle({
						selector: c.toggleSelector,
						context: this.$el
					}), "undefined" != typeof b && this._firstInit())
				},
				_firstInit: function() {
					var b = this.options;
					this.moduleMode = 0 === b.initialMode ? b.carouselWrapperId : b.mapWrapperId;
					var c = a(b.toggleSelector + "[data-target=" + this.moduleMode + "]", this.$el),
						d = {};
					d.target = c, b.monoMode ? (c.show(), this.$toggleSelectors.not(c).remove(), this._initMode()) : c.containerToggle("enableEl")
				},
				_bindEvents: function(b, c) {
					a(window).on("resize", a.Aperto.throttle(function() {
						b.moduleMode = c.carouselWrapperId, b._selectMode(c), b._toggleTabSelector(a("#tab-selector .carousel-map-module")), a("#tab-selector .carousel-map-module").trigger("click")
					})), this.$toggleSelectors.on("containerToggle:complete", this._initMode.bind(this)), this.$tabSelectors.on("click", this._onClickTab.bind(this)), a(c.zoomIn, this.$el).on("click touchstart", function(a) {
						a.preventDefault();
						var c;
						b.map && (c = b.map.getZoom(), 30 > c && b.map.setZoom(c + 1))
					}), a(c.zoomOut, this.$el).on("click touchstart", function(a) {
						a.preventDefault();
						var c;
						b.map && (c = b.map.getZoom(), c > 1 && b.map.setZoom(c - 1))
					})
				},
				_initMode: function(b) {
					var c = this.options,
						d = 0 === c.initialMode ? c.carouselWrapperId : c.mapWrapperId;
					this.moduleMode = b ? a(b.target).data("target") : d, this._toggleTabSelector(this.$tabSelectors.eq(this.selector)), this._selectMode(c)
				},
				_selectMode: function(a) {
					switch (this.moduleMode) {
						case a.mapWrapperId:
							this.mapMode();
							break;
						case a.carouselWrapperId:
						default:
							this.carouselMode()
					}
				},
				carouselMode: function() {
					this.isDebug && console.log("initialize carousel ", this), this._renderCarousel(this.selector)
				},
				mapMode: function() {
					this.isDebug && console.log("initialize map ", this), "object" != typeof google || "object" != typeof google.maps || this.mapRendered ? this._renderCategoryMarkers() : this._initMap()
				},
				_onClickTab: function(b) {
					this.isDebug && console.log("selector", b.target), b.preventDefault();
					var c = a(b.target),
						d = c.data("selector");
					c.is(".active") || (this.prevSelector = this.selector, this.selector = d, this._toggleLoader("on"), this._toggleTabSelector(c), this.moduleMode === this.options.carouselWrapperId ? this._renderCarousel(d) : this._renderCategoryMarkers())
				},
				_getData: function() {
					var b = this,
						c = this.$el.data("url");
					a.ajax({
						type: "GET",
						url: c,
						dataType: "json",
						success: function(a) {
							b.isDebug && console.log("success", a), b.data = a.data, b._firstInit()
						},
						error: function(a) {
							b.isDebug && console.error("error => ", a)
						}
					})
				},
				_renderCarousel: function(b) {
					var c = this,
						d = this.data[b];
					this.$carouselWrapper.html(this.templates.slidesList), this.$carousel = a(".carousel", this.$carouselWrapper), a.each(this.data, function(a, b) {
						c._getTabHelper(a).html(b.name)
					}), this._renderImages(d), this._toggleLoader("off"), this.$carousel.carousel()
				},
				_renderImages: function(b) {
					var c = "desktop" === this.mode ? b.slides : b.slides;
					a.each(c, function(b, c) {
						"undefined" != typeof c.link && (c.linkTitle = a.i18n.getText("carousel").link)
					});
					var d = this.slidesTemplate({
						slides: c
					});
					a(".carousel-area", this.$carousel).append(d)
				},
				_toggleTabSelector: function(a) {
					this.$tabSelectors.removeClass("active"), a.addClass("active")
				},
				_toggleLoader: function(a) {
					"on" === a ? this.$loader.addClass("active") : this.$loader.removeClass("active")
				},
				_getTabHelper: function(b) {
					return a(this.options.tabSelector + "[data-selector='" + b + "']")
				},
				_calculateHeight: function(a) {
					this._getDeviceMode(a);
					var b = "desktop" === this.mode ? a.desktopAspectRatio : a.mobileAspectRatio,
						c = this.viewportWidth * b;
					this.isDebug && console.log("resize _calculateHeight", c), console.log(c, b, this.viewportWidth), this.$carouselWrapper.height(c), this.$mapWrapper.height(c), this.$loader.css("margin", c / 2 + "px auto"), this.$loader.addClass("active")
				},
				_initMap: function(b) {
					var c = this,
						d = this.options,
						e = d.devmode || !window.magnoliaFrontendData ? "img/gmaps/pin.png" : window.magnoliaFrontendData.themePath + "/img/gmaps/pin.png";
					this.pinImg = new google.maps.MarkerImage(e), a.extend(d, {
						googleMaps: {
							zoom: 15,
							center: new google.maps.LatLng(47.559731, 7.592213),
							mapTypeId: google.maps.MapTypeId.ROADMAP,
							zoomControl: !1,
							scrollwheel: !1,
							panControl: !1,
							streetViewControl: !1,
							mapTypeControl: !1,
							zoomControlOptions: {
								style: google.maps.ZoomControlStyle.LARGE,
								position: google.maps.ControlPosition.LEFT_BOTTOM
							}
						}
					}), this._toggleLoader("on"), a.webshims.ready("WINDOWLOAD", function() {
						c.isDebug && console.log("render map"), c.map = new google.maps.Map(document.getElementById(d.mapCanvasId), d.googleMaps), c._bindGMapsEvents(b, d)
					})
				},
				_bindGMapsEvents: function() {
					var a = this;
					google.maps.event.addListenerOnce(this.map, "tilesloaded", function() {
						a._toggleLoader("off"), a.mapRendered = !0, a._renderCategoryMarkers()
					})
				},
				_renderCategoryMarkers: function(b) {
					this._removeInfowindow();
					var c = "undefined" == typeof b ? this.selector : a(b.target).data("selector");
					this.isDebug && console.log("Render Markers", b), this._createGoogleMapMarkers(c), this._clearMapMarkers(), this._renderMarkers(c), this._toggleLoader("off")
				},
				_renderMarkers: function(a) {
					this._setMapMarkers(a), this.map.fitBounds(this.dataBounds[a]), this.map.setZoom(this.map.getZoom() - 1)
				},
				_setMapMarkers: function(b) {
					var c = this;
					this.$googleMapLink || (this.$googleMapLink = a("[data-target=map-module-wrapper]", this.$el)), this.options.monoMode && this.$googleMapLink.hide(), a.each(this.dataMakers[b], function(a, b) {
						b.setMap(c.map)
					})
				},
				_createGoogleMapMarkers: function(b) {
					if ("undefined" == typeof this.dataMakers[b]) {
						this.dataMakers[b] = [];
						var c = this,
							d = new google.maps.LatLngBounds;
						a.each(this.data[b].markers, function(a, e) {
							var f = new google.maps.Marker({
								position: new google.maps.LatLng(e.latitude, e.longitude),
								title: e.title,
								content: e.content,
								icon: c.pinImg,
								img: e.img,
								url: e.url
							});
							d.extend(f.getPosition()), c.dataMakers[b].push(f), c._bindGMapsInfowindowEvents(f)
						}), this.dataBounds[b] = d
					}
				},
				_clearMapMarkers: function() {
					var b = this.dataMakers[this.prevSelector];
					"undefined" != typeof this.prevSelector && "undefined" != typeof b && a.each(b, function(a, b) {
						b.setMap(null)
					})
				},
				_setInfowindowContent: function(a) {
					this._removeInfowindow();
					var b = {
							img: a.img,
							title: a.title,
							content: a.content,
							url: a.url
						},
						c = "desktop" === this.mode,
						d = c ? this.infowindowTemplate : this.infowindowMobileTemplate;
					this.infowindow = new InfoBox({
						latlng: a.getPosition(),
						map: this.map,
						content: d(b),
						width: 300,
						height: c ? 374 : 172
					})
				},
				_removeInfowindow: function() {
					this.infowindow && this.infowindow.remove()
				},
				_bindGMapsInfowindowEvents: function(a) {
					var b = this;
					google.maps.event.addListener(a, "click", function() {
						b._setInfowindowContent(a), b.options.monoMode && b._setLinkLocation(a)
					})
				},
				_setLinkLocation: function(b) {
					var c = b.getPosition();
					this.$googleMapLink.text(a.i18n.getText("maps").linkText).attr("target", "_blank").attr("href", "https://maps.google.com/maps?daddr=" + c.d + "," + c.e), this.$googleMapLink.show()
				},
				_getDeviceMode: function(a) {
					this.viewportWidth = window.innerWidth, this.mode = a.mobileWidth < this.viewportWidth ? "desktop" : "mobile"
				}
			})
		}(window.webshims && webshims.$ || jQuery),
		function(a) {
			"use strict";
			a.widget("aperto.locationfinderNew", {
				options: {
					button: "#maplink",
					map: "#map-canvas-wrapper",
					nav: "#category-list",
					animate: !0
				},
				_create: function() {
					this.isIE9 = a.browser.msie && "9.0" == a.browser.version ? !0 : !1, this.oldWidth = window.innerWidth, this.button = a(this.options.button), this.panel = this.element, this.locationsJson = this.panel.data("url"), this.isOpen = !1, this.map = a(this.options.map, this.panel), this.nav = a(this.options.nav, this.panel), this.mapCanvas, this.init = !0, this.loader = a(".loading", this.element), this.zoomInBtn = a(".zoom-in", this.element), this.zoomOutBtn = a(".zoom-out", this.element), this.infowindowTemplate = Handlebars.compile(window.templates.infowindowNEW), this.listItemTemplate = Handlebars.compile(window.templates["lf-list-item"]), this.markerItemTemplate = Handlebars.compile(window.templates["lf-marker-item"]), this.button.attr("disabeld", "disabeld"), this._getLocations(), this.closeBtn = a('<a class="close" title="' + a.i18n.getText("close") + '" href="#">' + a.i18n.getText("close") + "</a>").appendTo(this.panel), this.mapCloseBtn = a(".close-button", this.map), "object" == typeof google && "object" == typeof google.maps ? this._createMapSettings() : (this.element.addClass("error-no-map"), this._showError()), this._bindEvents()
				},
				_createMapSettings: function() {
					this.markers = [], this.dataBounds = new google.maps.LatLngBounds, this.infoWindows = [], this.firstInitMap = !1, a.extend(this.options, {
						googleMaps: {
							zoom: 10,
							center: new google.maps.LatLng(47.559731, 7.592213),
							mapTypeId: google.maps.MapTypeId.ROADMAP,
							zoomControl: !1,
							panControl: !1,
							streetViewControl: !1,
							mapTypeControl: !1,
							scrollwheel: !1
						}
					});
					var b = window.magnoliaFrontendData ? window.magnoliaFrontendData.themePath + "/img/gmaps/pin.png" : "img/gmaps/pin.png",
						c = window.magnoliaFrontendData ? window.magnoliaFrontendData.themePath + "/img/gmaps/main-pin.png" : "img/gmaps/main-pin.png";
					this.pinImg = new google.maps.MarkerImage(b), this.mainPinImg = new google.maps.MarkerImage(c), this.currentZoom = this.options.googleMaps.zoom
				},
				_bindEvents: function() {
					var b = this;
					this.button.on("click", function(a) {
						a.preventDefault(), b.isOpen ? b.closePanel() : b.openPanel()
					}), this.closeBtn.on("click", function(a) {
						a.preventDefault(), b.closePanel()
					}), this.mapCloseBtn.on("click", function(a) {
						a.preventDefault(), b.closeMapView()
					}), this.zoomInBtn.on("click", function(a) {
						a.preventDefault(), b.currentZoom = b.gMap.getZoom(), b.currentZoom < 30 && (b.gMap.setZoom(b.currentZoom + 1), b.currentZoom += 1)
					}), this.zoomOutBtn.on("click", function(a) {
						a.preventDefault(), b.currentZoom = b.gMap.getZoom(), b.currentZoom > 1 && (b.gMap.setZoom(b.currentZoom - 1), b.currentZoom -= 1)
					}), a(window).on("resize emchange", a.Aperto.throttle(function(a) {
						b.init || b.isIE9 || b.oldWidth === window.innerWidth || (a.preventDefault(), b.closeAllNavPanel(), b.closeAllInfo(), b.hideAllMarker(), b._removeInfoWindow(), b._clearMap(), b.nav.removeClass("is-hidden"), b.map.removeClass("is-visible"), b.mapCanvas && b.mapCanvas.removeAttr("style"), b.checkForMobileMode() ? b.panel.css({
							height: ""
						}) : (b.map.css({
							height: "",
							width: ""
						}), b.mapCanvas && b.mapCanvas.removeAttr("style"), b._initMap(), google.maps.event.trigger(b.gMap, "resize"), b.panel.css({
							height: b.calcPanelHeight()
						})), b.oldWidth = window.innerWidth), b.init = !1
					}))
				},
				closePanel: function() {
					this.panel.removeClass("is-visible").css({
						height: ""
					}), this.button.removeClass("is-active"), this.closeAllInfo(), this.closeAllNavPanel(), this.isOpen = !1
				},
				openPanel: function() {
					a("#introductions.active").trigger("click"), this.panel.addClass("is-visible"), this.checkForMobileMode() || (this.panel.css({
						height: this.calcPanelHeight()
					}), this.firstInitMap || ("undefined" == typeof google ? this._showError() : (this.loader.addClass("js-visible"), this.map.css({
						display: "block"
					}), this._initMap())), this.navBtns && this.openNavPanel(a(this.navBtns[0]), a(this.navPans[0]))), this.button.addClass("is-active"), this.isOpen = !0
				},
				calcPanelHeight: function() {
					var b = window.innerHeight - a("header").outerHeight();
					return b
				},
				checkForMobileMode: function() {
					var a = Modernizr.mq("(max-width: 767px)") ? 1 : 0;
					return a
				},
				_buildNav: function() {
					for (var b = a("<ul></ul>"), c = a("<nav></nav>"), d = 0; d < this.locations.length; d++) {
						var e = a('<li class="lf-category"></li>'),
							f = '<a id="' + this.locations[d].id + '" href="#" >' + this.locations[d].name + "</a>";
						if (a(f).appendTo(e), "cluster" === this.locations[d].type && this.locations[d].clusterItems) {
							for (var g = a("<ul></ul>"), h = 0; h < this.locations[d].clusterItems.length; h++) {
								for (var i = a("<li></li>"), j = this.locations[d].clusterItems[h], k = j.markers, l = 0; l < k.length; l++) {
									var m = this.markerItemTemplate(k[l]);
									a(m).appendTo(i)
								}
								var n = this.listItemTemplate(j);
								a(n).prependTo(i), i.appendTo(g)
							}
							g.appendTo(e)
						} else if ("marker" === this.locations[d].type && this.locations[d].markers) {
							for (var g = a("<ul></ul>"), h = 0; h < this.locations[d].markers.length; h++) {
								var i = a("<li></li>"),
									o = this.locations[d].markers[h],
									n = this.listItemTemplate(o);
								a(n).appendTo(i), i.appendTo(g)
							}
							g.appendTo(e)
						}
						e.appendTo(b)
					}
					b.appendTo(c), this.nav.html(c), this.button.removeAttr("disabeld"), this._bindNavEvents()
				},
				_getLocations: function() {
					var b = this;
					a.ajax({
						type: "GET",
						url: b.locationsJson,
						dataType: "json",
						success: function(a) {
							b.locations = a.data, b._buildNav()
						},
						error: function() {
							b._showError()
						}
					})
				},
				_showError: function() {
					this.nav.find(".error-msg").length < 1 && a('<p class="error-msg">' + a.i18n.getText("errorMsg") + "</p>").appendTo(this.nav)
				},
				_bindNavEvents: function() {
					var b = this;
					this.navBtns = a("ul > li.lf-category > a", this.nav), this.navPans = a("ul > li > ul", this.nav), this.navBtns.on("click", function(c) {
						c.preventDefault();
						var d = b.navBtns.index(this);
						a(this).is(".is-active") ? b.closeNavPanel(a(this), a(b.navPans[d])) : b.openNavPanel(a(this), a(b.navPans[d]))
					}), this.markerBtns = a("ul ul li a.marker", this.nav), this.markerBtns.on("click", function(c) {
						c.preventDefault();
						var d = a(this);
						b.checkForMobileMode() ? d.is(".is-active") ? b.closeInfo(d) : b.openInfo(d) : (b.markerBtns.removeClass("is-active"), d.addClass("is-active"), d.setFocus(), b.showAllMarker(d.parent().find(".marker"), !1))
					})
				},
				closeNavPanel: function(a, b) {
					a.removeClass("is-active"), b.removeClass("is-visible")
				},
				openNavPanel: function(a, b) {
					this.closeAllNavPanel(), this.closeAllInfo(), a.addClass("is-active"), a.setFocus(), b.addClass("is-visible"), this.checkForMobileMode() || "undefined" == typeof google || this.showAllMarker(b.find("a.marker"), !0)
				},
				closeAllNavPanel: function() {
					this.navBtns && this.navPans && (this.navBtns.removeClass("is-active"), this.navPans.removeClass("is-visible"))
				},
				_initMap: function() {
					var b = this,
						c = this.checkForMobileMode();
					if (c) var d = Math.round(.75 * a(window).outerHeight());
					else var d = 1 * (this.calcPanelHeight() - this.nav.outerHeight());
					var e = 1 * a(window).outerWidth(),
						f = {
							display: "block",
							height: d,
							width: e
						};
					this.map.css(f), this.mapCanvas = a("#map-canvas", this.map).css(f), b.gMap = new google.maps.Map(document.getElementById("map-canvas"), b.options.googleMaps), google.maps.event.addListenerOnce(this.gMap, "tilesloaded", function() {
						b.loader.removeClass("js-visible"), b.map.css({
							display: ""
						})
					}), this.isIE9 && google.maps.event.trigger(b.gMap, "resize"), this.firstInitMap = !0
				},
				openInfo: function(b) {
					var c = this;
					this.closeAllInfo(), b.addClass("is-active"), b.setFocus(), a(this.infowindowTemplate(b.data())).appendTo(b.parent()).addClass("is-visible"), a(".open-map", this.nav).on("click", function(b) {
						b.preventDefault(), c.openMapView(), c.showAllMarker(a(this).closest("li").find(".marker"), !1)
					})
				},
				closeInfo: function(b) {
					a(".infowindow-content.is-visible").remove(), b.removeClass("is-active")
				},
				closeAllInfo: function() {
					this.navPans && this.navPans.find(".is-active").removeClass("is-active"), this.markerBtns && this.markerBtns.removeClass("is-active"), a(".infowindow-content.is-visible").remove()
				},
				openMapView: function() {
					this.firstInitMap || (this.loader.addClass("js-visible"), void 0 === typeof google ? this._showError() : this._initMap()), this.map.addClass("is-visible"), this.nav.addClass("is-hidden")
				},
				closeMapView: function() {
					this.hideAllMarker(), this.map.removeClass("is-visible"), this.nav.removeClass("is-hidden")
				},
				showMarker: function(a) {
					this.hideAllMarker(), this._createMarker(a, !1)
				},
				hideMarker: function(a) {
					this._removeInfoWindow(), a.setMap(null)
				},
				hideAllMarker: function() {
					this._removeInfoWindow();
					var a = this.markers;
					if (0 != a.length)
						for (var b = 0; b < a.length; b++) a[b].setMap(null)
				},
				showAllMarker: function(b, c) {
					var d = this;
					this.hideAllMarker(), this.dataBounds = new google.maps.LatLngBounds, b.each(function() {
						d._createMarker(a(this).data(), a(this).is("a"), c)
					}), this.gMap.fitBounds(this.dataBounds), this.gMap.getZoom() > 17 && this.gMap.setZoom(17)
				},
				_createMarker: function(b, c, d) {
					var e = this,
						f = new google.maps.Marker({
							position: new google.maps.LatLng(b.lat, b.long),
							title: b.title,
							content: b.content,
							icon: c ? e.mainPinImg : e.pinImg,
							img: b.img,
							map: e.gMap,
							url: b.url,
							id: b.id
						});
					this.markers.push(f), this.dataBounds.extend(f.getPosition()), this.checkForMobileMode() ? google.maps.event.addListener(f, "click", function() {
						e.openInfo(a("#" + this.id, e.nav)), e.closeMapView()
					}) : google.maps.event.addListener(f, "click", function() {
						e._removeInfoWindow(), e._createInfoWindow(this)
					}), c && (d || this.checkForMobileMode() ? this.gMap.setCenter(f.getPosition()) : (this.gMap.setCenter(f.getPosition()), this._createInfoWindow(f)))
				},
				_removeInfoWindow: function() {
					this.infoWindow && this.infoWindow.remove()
				},
				_createInfoWindow: function(b) {
					var c = this;
					this.infoWindow = new InfoBox({
						latlng: b.getPosition(),
						map: this.gMap,
						content: this.infowindowTemplate(b),
						width: 300,
						height: 550
					}), a(this.infoWindow).find("a.close").on("click", function(a) {
						a.preventDefault(), c._removeInfoWindow()
					}), c.closeAllNavPanel()
				},
				_clearMap: function() {
					this.map.css({
						height: "",
						width: ""
					}), this.mapCanvas && this.mapCanvas.removeAttr("style").empty(), this.firstInitMap = !1
				}
			})
		}(window.webshims && webshims.$ || jQuery),
		function(a) {
			"use strict";
			a.widget("aperto.organizationFinder", {
				options: {
					isDebug: !1,
					handle: ".toggle",
					expandable: ".expandable_content",
					mapMainWrapper: ".main-map-wrapper",
					map: ".map",
					mapWrapper: ".map-wrapper",
					loader: ".loading",
					loaderImg: ".loader",
					pinIcon: "../img/gmaps/pin.png",
					preselected: 0
				},
				_create: function() {
					var b = this.options;
					this.isDebug = b.isDebug, this.isDebug && console.log("initialize ", this), this.$el = a(this.element), this.$mapCanvas = a(b.map, this.$el), this.$mapClone = this.$mapCanvas.clone(), this.rendered = !1, "undefined" == typeof this.$mapCanvas || this.$mapCanvas.size() < 1 || this._bindEvents()
				},
				_initMapElements: function() {
					if ("undefined" != typeof google && "undefinde" != typeof google.maps) {
						var b = this.options;
						this.rendered = !0;
						var c = b.devmode || !window.magnoliaFrontendData ? "img/gmaps/pin.png" : window.magnoliaFrontendData.themePath + "/img/gmaps/pin.png";
						this.pinImg = new google.maps.MarkerImage(c), this._getDeviceMode(b), this._calculateCanvasSize(), a.extend(b, {
							googleMaps: {
								zoom: 15,
								center: new google.maps.LatLng(47.5667, 7.6),
								mapTypeId: google.maps.MapTypeId.ROADMAP,
								disableDefaultUI: !0
							}
						}), b.googleMaps.center = new google.maps.LatLng(this.$mapCanvas.data("lat"), this.$mapCanvas.data("lon")), this.map = new google.maps.Map(this.$mapCanvas[0], b.googleMaps), this._bindGMapsEvents(), this._setMapMarker()
					} else this._showError()
				},
				_showError: function() {
					a('<p class="hint-error">' + a.i18n.getText("errorMsg") + "</p>").insertAfter(a(this.options.mapMainWrapper, this.element)), a(this.options.mapMainWrapper, this.element).addClass("hint-error-wrapper")
				},
				_bindEvents: function() {
					var b = this;
					a(window).smartresize(function() {
						b.rendered && b._resetCanvas()
					}), this.$el.on("panelExpanded", function() {
						b._initMapElements()
					}), this.$el.on("containerToggle:start", function(c) {
						b.isDebug && console.log("containerToggle:start", c), a(this).on("containerToggle:complete", function(c) {
							b.isDebug && console.log("containerToggle:complete", c), a.webshims.ready("WINDOWLOAD", function() {
								b._initMapElements()
							}), a(this).off("containerToggle:complete")
						})
					})
				},
				_bindGMapsEvents: function() {
					var b = this;
					google.maps.event.addListenerOnce(this.map, "tilesloaded", function() {
						a(b.options.loader, b.$el).removeClass("active")
					})
				},
				_setMapMarker: function() {
					new google.maps.Marker({
						position: this.options.googleMaps.center,
						icon: this.pinImg,
						map: this.map
					})
				},
				_calculateCanvasSize: function() {
					var b, c = this.options,
						d = "mobile" === this.mode ? 1 : 0,
						e = d ? parseInt(this.$mapCanvas.css("padding-left").replace("px", ""), 10) : 0,
						f = a(c.mapMainWrapper, this.$el),
						g = f.css("float"),
						h = a(c.loaderImg, this.$el).width();
					f.css("float", "none"), b = this.$mapCanvas.width() - 2 * e, f.css("float", g), a(this.$mapCanvas, this.$el).width(b).height(b), d && a(this.$mapCanvas, this.$el).css("margin-left", e), a(c.loader, this.$el).css("margin", b / 2 - h / 2 + "px auto")
				},
				_resetCanvas: function() {
					var b = this.options;
					this.$mapCanvas.remove(), this.$mapClone.attr("style", ""), this.isDebug && console.log("Cloned Map El", this.$mapClone[0]), a(b.mapWrapper, this.$el).html(this.$mapClone), this.$mapCanvas = a(this.options.map, this.$el), this._initMapElements()
				},
				_getDeviceMode: function(a) {
					this.mode = a.mobileWidth >= window.innerWidth ? "mobile" : "desktop"
				}
			})
		}(window.webshims && webshims.$ || jQuery),
		function(a) {
			jspackager && jspackager.devmode, "undefined" == typeof window.aperto && (window.aperto = {});
			var b = {
					immediate: function() {},
					domReadyOnce: function() {
						window.HandlerbarsTemplates()
					},
					everyDomReady: function(b) {
						c(b), a("#locationfinder", b).locationfinderNew(), a("#result-list > li", b).organizationFinder()
					}
				},
				c = function(b) {
					a(".living:not(.map)", b).carouselMapModule({
						tabSelector: ".tab-selector"
					}), a(".living.map", b).carouselMapModule({
						monoMode: !0,
						initialMode: 1,
						tabSelector: ".tab-selector",
						mobileAspectRatio: .75
					})
				};
			b.immediate(), a(b.domReadyOnce), a(function() {
				b.everyDomReady(document), a(document).on("dommodified", function(a) {
					b.everyDomReady(a.target)
				})
			})
		}(window.webshims && webshims.$ || jQuery);
}
