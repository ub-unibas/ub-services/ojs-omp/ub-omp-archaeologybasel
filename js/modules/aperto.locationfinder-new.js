/* Author: Mirjam Guderley */
(function($) {
	"use strict";
	$.widget('aperto.locationfinderNew', {
		options: {
			button: '#maplink',
			map: '#map-canvas-wrapper',
			nav: '#category-list',
			animate: true
		},
		_create: function() {
			// Declare Variables
			//console.log('_create');

			this.isIE9 = ($.browser.msie && $.browser.version == "9.0") ? true : false;
			this.oldWidth = window.innerWidth;
			//console.log(this.isIE9);
			this.button = $(this.options.button);
			this.panel = this.element;
			this.locationsJson = this.panel.data('url');
			this.isOpen = false;
			this.map = $(this.options.map, this.panel);
			this.nav = $(this.options.nav, this.panel);
			this.mapCanvas;

			this.init = true;
			this.loader = $('.loading', this.element);

			// ZOOM
			this.zoomInBtn = $('.zoom-in', this.element);
			this.zoomOutBtn = $('.zoom-out', this.element);


			//TEMPLATES
			this.infowindowTemplate = Handlebars.compile(window.templates['infowindowNEW']);
			this.listItemTemplate = Handlebars.compile(window.templates['lf-list-item']);
			this.markerItemTemplate = Handlebars.compile(window.templates['lf-marker-item']);

			//GET LOCATIONS
			this.button.attr('disabeld', 'disabeld');
			this._getLocations();

			// Close-Btn
			this.closeBtn = $('<a class="close" title="' + $.i18n.getText('close') + '" href="#">' + $.i18n.getText('close') + '</a>').appendTo(this.panel);
			this.mapCloseBtn = $('.close-button', this.map);

			if (typeof(google) === 'object' && typeof(google.maps) === 'object') {
				this._createMapSettings();
			} else {
				this.element.addClass('error-no-map');
				this._showError();
			}

			// Bind Events
			this._bindEvents();
		},
		_createMapSettings: function() {

			//console.log('_createMapSettings');
			this.markers = [];
			this.dataBounds = new google.maps.LatLngBounds();;
			this.infoWindows = [];
			this.firstInitMap = false;

			$.extend(this.options, {
				googleMaps: {
					zoom: 10,
					center: new google.maps.LatLng(47.559731, 7.592213),
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					zoomControl: false,
					panControl: false,
					streetViewControl: false,
					mapTypeControl: false,
					scrollwheel: false,
				}
			});

			var pinIcon = !window.magnoliaFrontendData ? 'img/gmaps/pin.png' : window.magnoliaFrontendData.themePath + '/img/gmaps/pin.png';
			var mainPinIcon = !window.magnoliaFrontendData ? 'img/gmaps/main-pin.png' : window.magnoliaFrontendData.themePath + '/img/gmaps/main-pin.png';
			this.pinImg = new google.maps.MarkerImage(pinIcon);
			this.mainPinImg = new google.maps.MarkerImage(mainPinIcon);


			this.currentZoom = this.options.googleMaps.zoom;
		},
		_bindEvents: function() {
			//console.log('_bindEvents');
			var that = this;
			this.button.on('click', function(e) {
				e.preventDefault();
				if (that.isOpen) {
					that.closePanel();
				} else {
					that.openPanel();
				}
			});
			this.closeBtn.on('click', function(e) {
				e.preventDefault();
				that.closePanel();
			});
			this.mapCloseBtn.on('click', function(e) {
				e.preventDefault();
				that.closeMapView();
			});
			this.zoomInBtn.on('click', function(e) {
				e.preventDefault();
				that.currentZoom = that.gMap.getZoom();
				if (that.currentZoom < 30) {
					that.gMap.setZoom(that.currentZoom + 1);
					that.currentZoom += 1;
				}
			});
			this.zoomOutBtn.on('click', function(e) {
				e.preventDefault();
				that.currentZoom = that.gMap.getZoom();
				if (that.currentZoom > 1) {
					that.gMap.setZoom(that.currentZoom - 1);
					that.currentZoom -= 1;
				}
			});
			$(window).on('resize emchange', $.Aperto.throttle(function(e) {
				//console.log('_resize', (!that.init && !that.isIE9));
				if (!that.init && !that.isIE9 && that.oldWidth !== window.innerWidth) {

					e.preventDefault();

					that.closeAllNavPanel();
					that.closeAllInfo();
					that.hideAllMarker();
					that._removeInfoWindow();
					that._clearMap();
					that.nav.removeClass('is-hidden');
					that.map.removeClass('is-visible');
					if (that.mapCanvas) {
						that.mapCanvas.removeAttr('style');
					}

					if (that.checkForMobileMode()) {
						that.panel.css({
							height: ''
						});
					} else {
						that.map.css({
							height: '',
							width: ''
						});
						if (that.mapCanvas) {
							that.mapCanvas.removeAttr('style');
						}
						that._initMap();
						google.maps.event.trigger(that.gMap, 'resize');
						that.panel.css({
							height: that.calcPanelHeight()
						});
					}

					that.oldWidth = window.innerWidth;
				}
				that.init = false;
			}));
		},
		closePanel: function() {
			//console.log('closePanel');
			this.panel.removeClass('is-visible').css({
				height: ''
			});
			this.button.removeClass('is-active');
			this.closeAllInfo();
			this.closeAllNavPanel();
			this.isOpen = false;
		},
		openPanel: function() {
			//console.log('openPanel');
			//CLOSE INTRODUCTION; ON OPENING
			$('#introductions.active').trigger('click');
			this.panel.addClass('is-visible');



			//DESKTOP
			if (!this.checkForMobileMode()) {
				this.panel.css({
					height: this.calcPanelHeight()
				});


				if (!this.firstInitMap) {
					if (typeof(google) === 'undefined') {
						this._showError();
					} else {
						this.loader.addClass('js-visible')
						this.map.css({
							display: 'block'
						});
						this._initMap();
					}
				}
				if (this.navBtns) {
					this.openNavPanel($(this.navBtns[0]), $(this.navPans[0]));
				}
			}

			this.button.addClass('is-active');
			//btn.setFocus();

			this.isOpen = true;
		},
		calcPanelHeight: function() {
			var height = window.innerHeight - $('header').outerHeight();
			return height;
		},
		checkForMobileMode: function() {
			var isMobile = ((Modernizr.mq('(max-width: 767px)'))) ? 1 : 0;
			return isMobile;
		},
		_buildNav: function() {
			var nav = $('<ul></ul>');
			var wrapper = $('<nav></nav>');

			for (var i = 0; i < this.locations.length; i++) {

				var listItem = $('<li class="lf-category"></li>');
				var link = '<a id="' + this.locations[i].id + '" href="#" >' + this.locations[i].name + '</a>';

				$(link).appendTo(listItem);

				//
				if (this.locations[i].type === 'cluster' && this.locations[i].clusterItems) {
					var subnav = $('<ul></ul>');
					for (var j = 0; j < this.locations[i].clusterItems.length; j++) {
						var innerlistItem = $('<li></li>');
						var clusterItem = this.locations[i].clusterItems[j];

						var clusterMarkers = clusterItem.markers;
						for (var k = 0; k < clusterMarkers.length; k++) {
							var innerInnerlink = this.markerItemTemplate(clusterMarkers[k]);
							$(innerInnerlink).appendTo(innerlistItem);
						}

						var innerlink = this.listItemTemplate(clusterItem);
						$(innerlink).prependTo(innerlistItem);
						innerlistItem.appendTo(subnav);
					}
					subnav.appendTo(listItem);
				} else if (this.locations[i].type === 'marker' && this.locations[i].markers) {

					var subnav = $('<ul></ul>');

					for (var j = 0; j < this.locations[i].markers.length; j++) {
						var innerlistItem = $('<li></li>');
						var helper = this.locations[i].markers[j];
						var innerlink = this.listItemTemplate(helper);
						$(innerlink).appendTo(innerlistItem);
						innerlistItem.appendTo(subnav);
					}

					subnav.appendTo(listItem);
				}

				listItem.appendTo(nav);
			}

			nav.appendTo(wrapper);
			this.nav.html(wrapper);

			//enable Button
			this.button.removeAttr('disabeld');
			this._bindNavEvents();
		},
		_getLocations: function() {
			var that = this;
			$.ajax({
				type: "GET",
				url: that.locationsJson,
				dataType: "json",
				success: function(data, textStatus, jqXHR) {
					that.locations = data.data;
					that._buildNav();
				},
				error: function(error) {
					that._showError();
				}
			});
		},
		_showError: function() {
			if (this.nav.find('.error-msg').length < 1) {
				$('<p class="error-msg">' + $.i18n.getText('errorMsg') + '</p>').appendTo(this.nav);
			}
		},
		_bindNavEvents: function() {
			//console.log('_bindNavEvents');
			var that = this;

			this.navBtns = $('ul > li.lf-category > a', this.nav);
			this.navPans = $('ul > li > ul', this.nav);

			this.navBtns.on('click', function(e) {
				e.preventDefault();
				var idx = that.navBtns.index(this);
				if ($(this).is('.is-active')) {
					that.closeNavPanel($(this), $(that.navPans[idx]));
				} else {
					that.openNavPanel($(this), $(that.navPans[idx]));
				}
			});

			this.markerBtns = $('ul ul li a.marker', this.nav);

			this.markerBtns.on('click', function(e) {
				e.preventDefault();
				var btn = $(this);

				if (that.checkForMobileMode()) {
					if (btn.is('.is-active')) {
						that.closeInfo(btn);
					} else {
						that.openInfo(btn);
					}
				} else {
					that.markerBtns.removeClass('is-active');
					btn.addClass('is-active');
					btn.setFocus();
					that.showAllMarker(btn.parent().find('.marker'), false);
					//that._createInfoWindow($(btn).data());
				}
			});
		},
		closeNavPanel: function(btn, pan) {
			//console.log('closeNavPanel');
			btn.removeClass('is-active');
			pan.removeClass('is-visible');
		},
		openNavPanel: function(btn, pan) {
			//console.log('openNavPanel');
			this.closeAllNavPanel();
			this.closeAllInfo();
			btn.addClass('is-active');
			btn.setFocus();
			pan.addClass('is-visible');
			if (!this.checkForMobileMode() && typeof(google) !== 'undefined') {
				this.showAllMarker(pan.find('a.marker'), true);
			}
		},
		closeAllNavPanel: function() {
			//console.log('closeAllNavPanel');
			if (this.navBtns && this.navPans) {
				this.navBtns.removeClass('is-active');
				this.navPans.removeClass('is-visible');
			}
		},
		_initMap: function() {
			//console.log('_initMap');
			var that = this;
			var isMobile = this.checkForMobileMode();


			if (isMobile) {
				var mapHeight = Math.round($(window).outerHeight() * (3 / 4));
			} else {
				var mapHeight = (this.calcPanelHeight() - this.nav.outerHeight()) * 1;
			}
			var mapWidth = $(window).outerWidth() * 1;

			var mapCSSOptions = {
				display: 'block',
				height: mapHeight,
				width: mapWidth
			}

			this.map.css(mapCSSOptions);

			this.mapCanvas = $('#map-canvas', this.map).css(mapCSSOptions);

			that.gMap = new google.maps.Map(document.getElementById('map-canvas'), that.options.googleMaps);

			google.maps.event.addListenerOnce(this.gMap, 'tilesloaded', function() {
				that.loader.removeClass('js-visible');
				that.map.css({
					'display': ''
				});
			});

			if (this.isIE9) {
				google.maps.event.trigger(that.gMap, 'resize');
			}
			this.firstInitMap = true;
		},
		openInfo: function(btn) {
			//console.log('openInfo');
			var that = this;

			this.closeAllInfo();

			btn.addClass('is-active');
			btn.setFocus();

			$(this.infowindowTemplate(btn.data())).appendTo(btn.parent()).addClass('is-visible');

			$('.open-map', this.nav).on('click', function(e) {
				e.preventDefault();
				that.openMapView();
				that.showAllMarker($(this).closest('li').find('.marker'), false);
			});
		},
		closeInfo: function(btn) {
			$('.infowindow-content.is-visible').remove();
			btn.removeClass('is-active');
		},
		closeAllInfo: function() {
			if (this.navPans) {
				this.navPans.find('.is-active').removeClass('is-active');
			}
			if (this.markerBtns) {
				this.markerBtns.removeClass('is-active');
			}
			$('.infowindow-content.is-visible').remove();
		},
		openMapView: function() {
			//console.log('openMapView');
			if (!this.firstInitMap) {
				this.loader.addClass('js-visible');
				if (typeof(google) === undefined) {
					this._showError();
				} else {
					this._initMap();
				}

			}
			this.map.addClass('is-visible');
			this.nav.addClass('is-hidden');
		},
		closeMapView: function() {
			//console.log('closeMapView');
			this.hideAllMarker();
			this.map.removeClass('is-visible');
			this.nav.removeClass('is-hidden');
		},
		showMarker: function(item) {
			this.hideAllMarker();
			this._createMarker(item, false);
		},
		hideMarker: function(marker) {
			this._removeInfoWindow();
			marker.setMap(null);
		},
		hideAllMarker: function() {
			this._removeInfoWindow();
			var helper = this.markers;
			if (helper.length != 0) {
				for (var i = 0; i < helper.length; i++) {
					helper[i].setMap(null);
				}
			}
		},
		showAllMarker: function(markers, isInit) {
			var that = this;
			this.hideAllMarker();
			this.dataBounds = new google.maps.LatLngBounds();

			markers.each(function() {
				that._createMarker($(this).data(), $(this).is('a'), isInit);
			});

			this.gMap.fitBounds(this.dataBounds);

			if (this.gMap.getZoom() > 17) {
				this.gMap.setZoom(17);
			}
		},
		_createMarker: function(item, isMainPin, isInit) {
			var that = this;
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(item.lat, item.long),
				title: item.title,
				content: item.content,
				icon: isMainPin ? that.mainPinImg : that.pinImg,
				img: item.img,
				map: that.gMap,
				url: item.url,
				id: item.id
			});
			this.markers.push(marker);

			this.dataBounds.extend(marker.getPosition());


			if (!this.checkForMobileMode()) {
				google.maps.event.addListener(marker, 'click', function(e) {
					that._removeInfoWindow();
					that._createInfoWindow(this);
				});
			} else {
				google.maps.event.addListener(marker, 'click', function(e) {
					that.openInfo($('#' + this.id, that.nav));
					that.closeMapView();
				});
			}
			if (isMainPin) {
				if (!isInit && !this.checkForMobileMode()) {
					this.gMap.setCenter(marker.getPosition());
					this._createInfoWindow(marker);
				} else {
					this.gMap.setCenter(marker.getPosition());

				}
			}
		},
		_removeInfoWindow: function() {
			if (this.infoWindow) {
				this.infoWindow.remove();
			}
		},
		_createInfoWindow: function(marker) {
			var that = this;
			this.infoWindow = new InfoBox({
				latlng: marker.getPosition(),
				map: this.gMap,
				content: this.infowindowTemplate(marker),
				width: 300,
				height: 550
			});
			$(this.infoWindow).find('a.close').on('click', function(e) {
				e.preventDefault();
				that._removeInfoWindow();
			});
			that.closeAllNavPanel();
		},
		_clearMap: function() {
			this.map.css({
				height: '',
				width: ''
			});
			if (this.mapCanvas) {
				this.mapCanvas.removeAttr('style').empty();
			}
			//this.mapCanvas.delete;
			this.firstInitMap = false;
		}
	});
})(window.webshims && webshims.$ || jQuery);
