(function($) {
	"use strict";

	$.widget('aperto.tabsCarousel', {
		options: {
			isDebug: false,
			carousel: ".simple_box",
			initialTab: 0,
			elements: ".toc li",
			tabClass: "tab-",
			navButtonWidth: 56,
			desktopWith: 1024,
			carouselOptions: {
				animation: "slide",
				slideshow: false,
				useCSS: true,
				touch: false,
				directionNav: true,
				controlNav: false,
				minItems: 3,
				maxItems: 3,
				move: 3,
				itemWidth: 100
			},
			toggleContainer: {
				selector: "li a:not(.flex-next, .flex-prev)",
				expandable: ".sectiongroup section",
				isDebug: false,
				activeClass: "js-selected",
				equalHeight: true,
				tabMode: true,
				focus: false,
				initialTab: 0,
				speed: 0
			},
			context: ".calender",
			additionalDiv: ".aktuelles .entry_list"
		},
		_create: function() {
			var that = this,
				o = this.options;

			this.isDebug = o.isDebug;

			this.$el = $(this.element);
			this.$carousel = $(o.carousel, this.$el);
			this.$elements = $(o.elements, this.$carousel);
			this.carouselInitialized = false;
			this.isNoEqualHeight = this.$el.is(".no_equal_height");

			this._addTabClass();
			// if the element has the class 'no_equal_height'
			// the content's height will not be equalized 
			if (this.isNoEqualHeight) o.toggleContainer.equalHeight = false;
			this.containerToggle = new aperto.ContainerToggle($.extend(o.toggleContainer, {
				context: o.context
			}));

			this._bindEvents(this);
			this._trigger('complete', this);
		},
		_bindEvents: function(that) {
			$(window).smartresize(function(event) {
				that._addTabClass();
				that._setCarouselWidth();
			});
			// set
			this.containerToggle.$el.on("containerToggle:height", this._setMaxHeight.bind(this));
		},
		_addTabClass: function() {
			var $elems = this.$elements;

			if (typeof $elems === "undefined" || $elems.size() < 1) return;

			var elemsCount = $elems.size(),
				o = this.options,
				ltFour = elemsCount <= 4,
				carouselClass = ltFour ? o.tabClass + elemsCount : o.tabClass + 4,
				navButtonWidth = this._calculateButtonWidth(o);

			$(".tabbing", this.$el).addClass(carouselClass);
			// init Carousel
			if (!ltFour) {
				//this.$elements.width(navButtonWidth);
				this._initCarousel(navButtonWidth);
			}
		},
		_calculateButtonWidth: function(o) {
			var width = this.$el.width(),
				navButtonWidth = $(".flex-prev").size() === 0 ? o.navButtonWidth : $(".flex-prev").width();

			this.isDebug && console.log("width => ", (width - 2 * navButtonWidth - 4) / 3)
			return (width - 2 * navButtonWidth - 4) / 3;
		},
		_setMaxHeight: function(event) {
			var o = this.options;
			var $elements = event.elements;
			var $list = this.element.find('.sectiongroup');
			var $additionalDiv = $(this.options.additionalDiv, this.element.closest('section.content_block'));

			if (this.isNoEqualHeight || $additionalDiv.size() === 0) return;

			if (window.innerWidth >= o.desktopWith) {
				var additionalDivHeight = $additionalDiv.height(),
					elementHeight = $elements.eq(0).height(),
					tabHeight = $('.carousel-tabs', this.element).height(),
					height = elementHeight + tabHeight;

				if (height < additionalDivHeight)
					height = additionalDivHeight;

				this.isDebug && console.log("Resulting Height => ", height);


				$list.height(height - tabHeight);
				$elements.css("height", "100%");
				$additionalDiv.height(height);
			} else {
				$list.css("height", "auto");
				$additionalDiv.css("height", "auto");
			}
		},
		// carousel functions
		_initCarousel: function(navButtonWidth) {
			this._setCarouselWidth();
			this.$carousel.carousel({
				pagination: false,
				view: '.carousel-tabs',
				doc: '.slides',
				itemSel: 'li > a',
				viewportBreaks: {
					800: 3,
					1270: 3,
					1600: 3,
					1900: 3
				}
			});
		},
		_setCarouselWidth: function() {
			var newW = Math.round(this.$carousel.outerWidth() - (2 * $(this.$carousel.find('button')[0]).outerWidth())) - 2;
			this.$carousel.find('.carousel-tabs').width(newW);
		}
	});

})(window.webshims && webshims.$ || jQuery);
