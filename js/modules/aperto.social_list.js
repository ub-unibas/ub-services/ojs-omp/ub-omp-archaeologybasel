/* Author: Jose Burgos */
(function($) {
	"use strict";
	/*-
	 * Social list
	 [ ]
	-*/
	$.widget("aperto.socialList", {
		options: {
			isDebug: false,
			mobileWidth: 768
		},
		_create: function() {
			var o = this.options;

			this.isDebug = o.isDebug;
			this.isDebug && console.log("initialize ", this);

			this.$el = $(this.element[0]);
			this.$elements = $("ul li", this.$el);
			this.$lastEl = this.$elements.last();

			this._bindEvents(this, o);
			this._init(o);
		},
		_bindEvents: function(that, o) {
			$(window).smartresize(function(event) {
				that._init(o, event);
			});
		},
		_mobileMode: function(o) {
			this.$lastEl.addClass("last");
			this._calculateListWidth(o);
		},
		_noMobileMode: function(o) {
			this.$lastEl.removeClass("last");
			this.$elements.attr("style", "");
		},
		_calculateListWidth: function(o) {
			var widthEl = this.$el.width(),
				widthListEl = this.$elements.eq(0).width(),
				listLenght = this.$elements.size(),
				// calculates list element right padding
				paddingListEl = (widthEl - widthListEl * listLenght) / (listLenght - 1);

			this.$elements.not(".last").css("padding-right", Math.floor(paddingListEl));
		},
		_init: function(o, event) {
			if (this._getDevice() === "mobile")
				this._mobileMode(o);
			else
				this._noMobileMode(o);
		},
		_getDevice: function() {
			return this.options.mobileWidth >= window.innerWidth ? "mobile" : "noMobile";
		}
	});
})(window.webshims && webshims.$ || jQuery);
