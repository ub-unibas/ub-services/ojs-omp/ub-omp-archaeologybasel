(function($) {
	"use strict";
	$.widget('aperto.weather', {
		options: {
			url: "http://api.wunderground.com/api/01c5dfdb86dcb610/conditions/lang:DE/q/Switzerland/Basel.json"
		},
		_create: function() {
			this.container = $('.condition', this.element);

			this.updateWeather();
		},
		updateWeather: function() {
			var that = this;
			$.ajax({
				url: this.options.url,
				dataType: 'jsonp',
				jsonp: 'callback'
			}).then(function(data) {
				that.adjustContainer(data);
			});
		},
		adjustContainer: function(data) {
			if (this.currentData) {
				this.container.removeClass(this.currentData.current_observation.icon);
			}
			this.currentData = data;
			this.container.addClass(data.current_observation.icon);
			$('strong', this.container).html((data.current_observation.temp_c | 0) + '<abbr title="Grad Celsius">°</abbr>');
		}
	})
})(window.webshims && webshims.$ || jQuery);
