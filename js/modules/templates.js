(function($) {

	window.HandlerbarsTemplates = function() {
			if (typeof window.templates === "undefined") window.templates = [];

			if ($("#locationfinder-template").size() > 0) window.templates["locationfinder"] = Handlebars.compile($("#locationfinder-template").html());
			// living - Map and Carousel templates
			if ($("#living-template").size() > 0) window.templates["living"] = Handlebars.compile($("#living-template").html());
			window.templates["living-slides"] = [

				'{{#each slides}}',
				'<li  class="carousel-item">',
				'<div class="carousel-item-inner">',
				'<img class="normal" src="{{src}}" alt="Bild" title="Bild" />',
				'<div class="grid-container">',
				'{{#if title}}',
				'<section class="info-wrapper">',
				'<h1>',
				'{{#if link}}',
				'<a class="title" href="{{link}}" title="{{linkTitle}}">{{title}}</a>',
				'{{else}}',
				'<span class="title">{{title}}</span>',
				'{{/if}}',
				'</h1>',

				'{{#if link}}',
				'<a href="{{link}}" title="{{linkTitle}}" class="more">' + $.i18n.getText('more') + '</a>',
				'{{/if}}',

				'</section>',
				'{{/if}}',
				'</div>',
				'</div>',
				'</li>',
				'{{/each}}'

			].join("\n");
			window.templates["lf-list-item"] = [
				'<a class="marker" id="{{id}}"  data-id="{{id}}" href="#" {{#if url}}data-url="{{url}}"{{/if}} data-long="{{longitude}}" {{#if name}}data-title="{{name}}"{{/if}} {{#if name}}data-name="{{name}}"{{/if}} {{#if content}}data-content="{{content}}"{{/if}} {{#if img}}data-img="{{img}}"{{/if}} data-lat="{{latitude}}">{{#if title}}{{title}}{{/if}}{{#if name}}{{name}}{{/if}}</a>'
			].join("\n");
			window.templates["lf-marker-item"] = [
				'<span class="marker" id="{{id}}" data-id="{{id}}" href="#" {{#if url}}data-url="{{url}}"{{/if}} data-long="{{longitude}}" {{#if name}}data-title="{{name}}"{{/if}} {{#if name}}data-name="{{name}}"{{/if}} {{#if content}}data-content="{{content}}"{{/if}} {{#if img}}data-img="{{img}}"{{/if}} data-lat="{{latitude}}">{{#if title}}{{title}}{{/if}}{{#if name}}{{name}}{{/if}}</span>'
			].join("\n");
			window.templates["video-iframe"] = [
				'<iframe width="{{width}}" height="{{height}}" src="{{href}}" allowfullscreen></iframe>'
			].join("\n");
			// google maps
			window.templates["infowindow"] = [
				'<div class="infowindow-content desktop">',
				'<a id="infowindow-close" class="close"></a>',
				'<img src="{{#infowindowImg img}}{{/infowindowImg}}"/>',
				'<h1>{{title}}</h1>',
				'<p>{{content}}</p>',
				'{{#if url }}<a href="{{url}}" class="button" target="_blank" title="{{title}}">' + $.i18n.getText('visit') + '</a>{{/if}}',
				'</div>'
			].join("\n");
			window.templates["infowindowNEW"] = [
				'<div class="infowindow-content">',
				'<a id="infowindow-close" class="close"></a>',
				'<img src="{{#infowindowImg img}}{{/infowindowImg}}"/>',
				'{{#if title }}<h1>{{title}}</h1>{{/if}}',
				'{{#if content }}<p>{{content}}</p>{{/if}}',
				'<a href="#" id="{{id}}" data-id="{{id}}" data-long="{{long}}" data-lat="{{lat}}" class="button open-map" title="{{title}}">Auf Karte anzeigen</a>',
				'{{#if url }}<a href="{{url}}" class="button" target="_blank" title="{{title}}">' + $.i18n.getText('visit') + '</a>{{/if}}',
				'</div>'
			].join("\n");
			window.templates["infowindow-mobile"] = [
				'<div class="infowindow-content mobile">',
				'<div id="infowindow-close" class="close"></div>',
				'<h1>{{title}}</h1>',
				'<p>{{content}}</p>',
				'{{#if url }}<a href="{{url}}" class="button" target="_blank" title="{{title}}">' + $.i18n.getText('visit') + '</a>{{/if}}',
				'</div>'
			].join("\n");
			// news social media templates
			window.templates["twitter"] = [
				'<div class="media_box">',
				'<div class="socialcontent twitter">',
				'<abbr class="timeago" title="{{#toISOString data.createdAt}}{{/toISOString}}"></abbr>',
				'<h4>',
				'<a title="' + $.i18n.getText('openNewTab') + '" href="{{data.user.profileUrl}}/status/{{id}}" target ="_blank">{{data.text}}</a>',
				'</h4>',
				'</div>',
				'<footer class="simple_box_footer">',
				'<a target="_blank" href="{{data.user.profileUrl}}" title="' + $.i18n.getText('twitter') + '" class="button wide">' + $.i18n.getText('twitter') + '</a>',
				'</footer>',
				'</div>'
			].join("\n");
			window.templates["facebook"] = [
				'<div class="media_box">',
				'<div class="image_wrapper">',
				'<a title="' + $.i18n.getText('openNewTab') + '" target="_blank" href="{{data.link}}">',
				'<img class="socialimg" src="{{data.picture}}" alt="{{data.message}}" />',
				'</a>',
				'</div>',
				'<div class="socialcontent facebook">',
				'<abbr class="timeago" title="{{#toISOString data.createdTime}}{{/toISOString}}"></abbr>',
				'<h4>',
				'<a title="' + $.i18n.getText('openNewTab') + '" target="_blank" href="{{data.link}}">{{data.message}}</a>',
				'</h4>',
				'</div>',
				'<footer class="simple_box_footer">',
				'<a target="_blank" href="https://www.facebook.com/{{data.from.id}}" title="' + $.i18n.getText('facebook') + '" class="button wide">' + $.i18n.getText('facebook') + '</a>',
				'</footer>',
				'</div>'
			].join("\n");
			window.templates["youtube"] = [
				'<div class="media_box">',
				'<div class="image_wrapper">',
				'<a title="' + $.i18n.getText('openNewTab') + '" href="{{data.embeddedWebPlayerUrl}}?autoplay=1" target="_blank">',
				'<img src="{{#thumbnails data.thumbnails}}{{/thumbnails}}" alt="{{data.title}}"/>',
				'</a>',
				'</div>',
				'<div class="socialcontent youtube">',
				'<abbr class="timeago" title="{{#toISOString data.createdTime}}{{/toISOString}}"></abbr>',
				'<h4><a title="' + $.i18n.getText('openNewTab') + '" href="{{data.embeddedWebPlayerUrl}}?autoplay=1" target="_blank">{{data.title}}</h4>',
				'</div>',
				'<footer class="simple_box_footer">',
				'<a target="_blank" href="{{data.channel}}" title="' + $.i18n.getText('youtube') + '" class="button wide">' + $.i18n.getText('youtube') + '</a>',
				'</footer>',
				'</div>'
			].join("\n");
			window.templates["instagram"] = [
				'<div class="media_box">',
				'<div class="image_wrapper">',
				'<a title="' + $.i18n.getText('openNewTab') + '" href="{{data.link}}" target="_blank">',
				'<img src="{{#thumbnails data.images.thumbnail}}{{/thumbnails}}" alt="{{data.caption.text}}" />',
				'</a>',
				'</div>',
				'<div class="socialcontent instagram">',
				'<abbr class="timeago" title="{{#toISOString data.caption.created_time}}{{/toISOString}}"></abbr>',
				'<h4><a title="' + $.i18n.getText('openNewTab') + '" href="{{data.link}}" target="_blank">{{data.caption.text}}</a></h4>',
				'</div>',
				'<footer class="simple_box_footer">',
				'<a target="_blank" href="{{profile.url}}" title="' + $.i18n.getText('instagram') + '" class="button wide">' + $.i18n.getText('instagram') + '</a>',
				'</footer>',
				'</div>'
			].join("\n");
			window.templates["documents"] = [
				'{{#each docs}}',
				'<section class="document_item">',
				'<h1><a class="download" href="{{link}}" title="{{fileExtension}} öffnet in neuem Fenster" target="_blank">{{fileName}} ({{fileExtension}}, {{fileSize}})</a></h1>',
				'<em>{{{lastModified}}}</em>',
				'<p>{{{description}}}</p>',
				'</section>',
				'{{/each}}'
			].join("\n");
		}
		// refactor
	Handlebars.registerHelper('toISOString', function(date, options) {
		var dateDate = new Date(date);
		return dateDate.toISOString();
	});

	Handlebars.registerHelper('thumbnails', function(thumbnails, options) {
		return thumbnails[0] ? thumbnails[0].imageUrl : thumbnails.url;
	});

	Handlebars.registerHelper('infowindowImg', function(img, options) {
		var placeholder = window.location.href.indexOf('devmode') >= 0 || !window.magnoliaFrontendData ? "img/gmaps/placeholder.png" : window.magnoliaFrontendData.themePath + "/img/gmaps/placeholder.png";
		return img ? img : placeholder;
	});
})(window.webshims && webshims.$ || jQuery);
