/* Author: Jose Burgos */
(function($) {
	"use strict";
	/*-
	 * 
	-*/
	$.widget('aperto.searchModule', {
		options: {
			isDebug: false
		},
		_create: function() {
			var that = this,
				o = this.options;

			this.isDebug = o.isDebug;

			this.$el = $(this.element[0]);
			this.$form = $("form", this.$el);
			this.$searchInput = $("input[type=search]", this.$el);

			this._bindEvents(that, o);
			this._initAutocomplete();
		},
		_bindEvents: function(that, o) {
			this.$searchInput.on('change', that.sendRequest.bind(this));

			this.$searchInput.on("keydown", function(event) {
				false && console.log("keydown", event);
				if (event.which === 13) {
					event.preventDefault();
					that.sendRequest();
				}
			});
		},
		sendRequest: function() {
			var that = this;
			var o = that.options;

			this.isDebug && console.log("Serialized Form:", this.$form.serialize());

			this.$form.submit();
		},
		_initAutocomplete: function() {
			// autocomplete functions
			var that = this;
			this.cache = {};

			this.$searchInput.autocomplete({
				position: {
					at: 'left bottom',
					my: 'left top'
				},
				autoResize: false,
				minLength: this.$searchInput.data('autocomplete-minlen') || 1,
				highlight: true,
				source: function(request, response) {
					var params = {},
						term = request.term;
					if (typeof that.cache[term] !== "undefined") {
						response(that.cache[term]);
					} else {
						that._getAutoCompleteAjax(term, response);
					}
				},
				change: function(event, ui) {
					if (ui.item) {
						that._checkTerm(ui.item.value);
					}
				},
				select: function(event, ui) {
					var valueHasChanged = that._checkTerm(ui.item.value);
					if (valueHasChanged) that.sendRequest();
				}
			});
		},
		_checkTerm: function(value) {
			var res = false;

			if (typeof this.lastTerm === "undefined" || this.lastTerm !== value) {
				this.lastTerm = value;
				// set the entire autocompleted value to the form input,
				// before serializing and sending to the server 
				this.$searchInput.val(value);

				res = true;
			}
			return res;
		},
		_setLastTerm: function(term) {
			$.data(this, 'list', {
				lastTerm: term,
				vals: this.cache[term]
			});
		},
		_getAutoCompleteAjax: function(term, res) {
			var that = this;
			var param = this.$searchInput.data('param') || 'term';
			var params = {};

			params[param] = term;

			$.ajax({
				url: this.$searchInput.data('source'),
				dataType: "json",
				data: params,
				success: function(data, status, xhr) {
					that.isDebug && console.log("success: ", data);
					// save in cache
					that.cache[term] = data.suggest;
					// return the results
					res(data.suggest);
				},
				error: function(error) {
					that.isDebug && console.log("error: ", error);
				}
			});
		}
	});
})(window.webshims && webshims.$ || jQuery);
