/**
 * @author trixta
 */
(function($) {
	var posMatrix = {
		left: 0,
		top: 0,
		center: 1,
		middle: 1,
		right: 2,
		bottom: 2,
		sameleft: 3,
		sametop: 3,
		sameright: 4,
		samebottom: 4
	};

	function getPosition(aroundElement, posElement, offset, type, pos, fitToView, fitToViewElem) {
		var params = (type == 'horizontal') ? ['Left', 'outerWidth', 'width'] : ['Top', 'outerHeight', 'height'],
			uOff = offset[params[0].toLowerCase()],
			posDim = posElement[params[1]](),
			flipped = {},
			viewPort = {},
			inViewFlip,
			ret;
		pos = isFinite(pos) ? pos : posMatrix[pos];

		switch (pos) {
			case 0:
				ret = uOff - posDim;
				break;
			case 1:
				ret = uOff + (aroundElement[params[1]]() / 2) - (posDim / 2);
				break;
			case 2:
				ret = uOff + aroundElement[params[1]]();
				break;
			case 3:
				ret = uOff;
				break;
			case 4:
				ret = uOff + aroundElement[params[1]]() - posDim;
				break;
		}

		function getView() {
			if (viewPort.start || viewPort.end) {
				return;
			}
			var offset;
			if (!fitToViewElem) {
				viewPort.start = $(window)['scroll' + params[0]]();
				viewPort.end = viewPort.start + $(window)[params[2]]();
			} else {
				fitToViewElem = $(fitToViewElem);
				viewPort.start = fitToViewElem.offset()[params[0].toLowerCase()];
				viewPort.end = viewPort.start + fitToViewElem[params[1]]();
			}

		}

		function inView(retPos, posType) {
			if (!fitToView) {
				return true;
			}
			getView();
			if (viewPort.start > retPos && posType < 2) {
				return 2;
			} else if (viewPort.end < retPos + posDim && posType > 0) {
				return 0;
			}
			return true;
		}

		inViewFlip = inView(ret, pos);
		if (inViewFlip !== true) {
			flipped.pos = inViewFlip;
			flipped.ret = getPosition(aroundElement, posElement, offset, type, flipped.pos, fitToViewElem)[0];
			if (inView(flipped.ret, flipped.pos) === true) {
				ret = flipped.ret;
				pos = flipped.pos;
			}
		}

		return [ret, pos];
	}

	$.posAround = function(posElement, aroundElement, o) {
		o = $.extend({}, $.posAround.defaults, o);
		posElement = $(posElement);
		var offset,
			css = {};
		if (o.offsetType == 'position') {
			o.fitToView = false;
		}
		if (isFinite(aroundElement.pageX) && isFinite(aroundElement.pageY)) {
			offset = {
				top: aroundElement.pageY,
				left: aroundElement.pageX
			};
			aroundElement.outerWidth = function() {
				return o.mouseWidth;
			};
			aroundElement.outerHeight = function() {
				return o.mouseHeight;
			};
		} else {
			aroundElement = $(aroundElement);

			offset = aroundElement[o.offsetType]();
		}

		css.left = getPosition(aroundElement, posElement, offset, 'horizontal', o.horizontal, (o.fitToView === true || o.fitToView == 'horizontal'), o.fitToViewElem);
		css.top = getPosition(aroundElement, posElement, offset, 'vertical', o.vertical, (o.fitToView === true || o.fitToView == 'vertical'), o.fitToViewElem);
		$.posAround.setPosClass(posElement, css);
		return css;

	};
	$.posAround.setPosClass = function(posElement, css) {
		$.posAround.cleanUpPosClass(posElement);
		posElement.addClass('positionaround-' + css.left[1] + '-' + css.top[1]);
		css.top = css.top[0];
		css.left = css.left[0];
	};
	$.posAround.cleanUpPosClass = function(posElement) {
		var classes = $.grep(posElement.attr('class').split(' '), function(classVal) {
			return (classVal.indexOf('positionaround-') !== 0);
		});
		posElement.attr('class', classes.join(' '));
	};
	$.posAround.defaults = {
		horizontal: 'right', //left | center | right | same[left|right]
		vertical: 'bottom', // bottom | middle | top | same[bottom|top]
		fitToView: true,
		mouseWidth: 15,
		mouseHeight: 20,
		fitToViewElem: false,
		offsetType: 'offset'
	};
})(window.webshims && webshims.$ || jQuery);
