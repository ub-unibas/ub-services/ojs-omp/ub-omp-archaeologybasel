/**
 * @author alexander.farkas
 */

(function($) {

	var posExclude = {
		absolute: /absolute/,
		relative: /absolute|fixed|relative/
	};

	function addStyles(jElm, styles) {
		var elem = jElm[0];
		if (!elem) {
			return jElm;
		}
		var elemStyle = elem.style;
		if (styles.position && posExclude[styles.position].test($.css(elem, 'position'))) {
			styles = $.extend({}, styles);
			delete styles.position;
		}
		$(jElm).css(styles);
		return jElm;
	}

	$.fn.fadeOver = function(opts) {
		if (!this[0]) {
			return this;
		}
		opts = $.extend({}, $.fn.fadeOver.defaults, opts);
		if ((this.length > 1 || opts.hideElement.length > 1)) {
			console.log('fadeOver: wir können immer nur ein hideElement bzw. ein showElement behandeln');
		}

		var jElm = $(this[0]),
			parentElement = (opts.parentSel) ? jElm.closest(opts.parentSel) : jElm.parent(),
			animInCSS = {
				start: {
					position: 'absolute',
					top: (parseInt(parentElement.css('paddingTop'), 10) || 0),
					left: (parseInt(parentElement.css('paddingLeft'), 10) || 0),
					right: 'auto',
					bottom: 'auto',
					display: 'block'
				},
				end: {},
				after: {
					position: '',
					top: '',
					left: '',
					width: '',
					right: '',
					bottom: ''
				}
			},
			animOutCSS = $.extend(true, {}, animInCSS, {
				after: {}
			}),
			parentStyles = {
				start: {
					position: 'relative',
					zoom: '1',
					overflow: 'hidden',
					height: Math.max(parentElement.height(), opts.hideElement.outerHeight(true)) + 'px'
				},
				end: {
					height: '',
					overflow: ''
				}
			};

		if (opts.stopAll) {
			opts.hideElement.stop(true, true);
			jElm.stop(true, true);
			parentElement.stop(true, true);
		}
		animInCSS.start.width = jElm.width();
		animOutCSS.start.width = opts.hideElement.width();

		opts.showElement = jElm;



		if (opts.hideStyle === 'visibility') {
			animInCSS.start.visibility = '';
			animOutCSS.after.visibility = 'hidden';
		} else {
			animInCSS.start.display = 'block';
			animOutCSS.after.display = 'none';
		}

		if (opts.inAnim) {
			$.each(opts.inAnim.split(','), function(i, anim) {
				$.fn.fadeOver.presets[anim](jElm, animInCSS, opts);
			});
		}

		if (opts.outAnim) {
			$.each(opts.outAnim.split(','), function(i, anim) {
				$.fn.fadeOver.presets[anim](opts.hideElement, animOutCSS, opts);
			});
		}

		if (!$.support.opacity) {
			if (opts.hideElement.css('zIndex') == 'auto' && !animOutCSS.start.zIndex) {
				animOutCSS.start.zIndex = '1';
				animOutCSS.after.zIndex = '';
			}
			if (jElm.css('zIndex') == 'auto' && !animInCSS.start.zIndex) {
				animInCSS.start.zIndex = '1';
				animInCSS.after.zIndex = '';
			}
		}

		if (opts.changeFxStyles) {
			opts.changeFxStyles({
				enter: {
					elem: jElm,
					css: animInCSS
				},
				leave: {
					elem: opts.hideElement,
					css: animOutCSS
				},
				parent: {
					elem: parentElement,
					css: parentStyles
				}
			}, opts);
		}

		addStyles(parentElement, parentStyles.start);

		var startFx = function() {
			var outerHeight = jElm.outerHeight(true);

			function animateParentHeight() {

				parentElement
					.animate({
							height: outerHeight
						},
						$.extend({}, opts.animOpts, {
							complete: function() {
								parentElement.css(parentStyles.end).removeClass('fadeover');
								opts.complete.apply(jElm[0]);
							}
						})
					);
			}

			addStyles(opts.hideElement, animOutCSS.start);

			if (opts.outAnim) {
				opts.hideElement.animate(animOutCSS.end, $.extend({}, opts.animOpts));
			}

			addStyles(jElm, animInCSS.start);

			parentElement.addClass('fadeover');

			if (opts.inAnim) {
				jElm.animate(animInCSS.end, $.extend({}, opts.animOpts, {
					complete: function() {
						if (opts.hideStyle === 'visibility') {
							opts.hideElement.parent().css({
								overflow: 'hidden',
								height: 0
							});
						}
						opts.hideElement.css(animOutCSS.after);
						jElm.css(animInCSS.after);
						if (!opts.animateHeight) {
							parentElement.css(parentStyles.end).removeClass('fadeover');
							opts.complete.apply(this, arguments);
						}

						if (opts.animateHeight == 'linear') {
							animateParentHeight();
						}
					}
				}));
			}
			if (opts.hideStyle === 'visibility') {
				jElm.parent().css('height', '');
			}

			if (opts.animateHeight === 'sync') {
				animateParentHeight();
			}
		};



		if (opts.stopAll) {
			startFx();
		} else {
			parentElement.queue(function() {
				parentElement.dequeue();
				startFx();
			});
		}

		return this;
	};

	$.fn.fadeOver.defaults = {
		animateHeight: false, // false | sync || linear
		inAnim: 'fadeIn',
		outAnim: 'fadeOut',
		hideStyle: 'display',
		parentSel: false,
		hideElement: $([]),
		stopAll: true,
		complete: $.noop,
		animOpts: {
			duration: 400
		}
	};

	$.fn.fadeOver.presets = {
		fadeIn: function(jElm, css, opts) {
			if (!jElm[0]) {
				return css;
			}
			if (jElm[0].offsetHeight < 3 || jElm[0].offsetWidth < 3 || jElm.css('visibility') === 'hidden') {
				css.start.opacity = 0;
			}
			css.end.opacity = 1;
			css.after.opacity = '';
			return css;
		},
		fadeOut: function(jElm, css, opts) {
			css.end.opacity = 0;
			return css;
		}
	};

	(function() {
		var dirOut = function(side, otherSide) {
			return function(jElm, css, opts) {
				css.start[otherSide] = 'auto';
				css.end[side] = jElm.outerWidth() * -1;
				return css;
			};
		};
		var dirIn = function(side, otherSide) {
			return function(jElm, css, opts) {
				if (!jElm[0]) {
					return css;
				}
				css.end[side] = css.start[side] || 0;
				css.start[side] = jElm.outerWidth() * -1;
				css.start[otherSide] = 'auto';
				css.end[side] = 0;
				css.after[side] = '';
				css.after[otherSide] = '';
				return css;
			};
		};

		$.extend($.fn.fadeOver.presets, {
			leftOut: dirOut('left', 'right'),
			rightOut: dirOut('right', 'left'),
			leftIn: dirIn('left', 'right'),
			rightIn: dirIn('right', 'left')
		});

	})();

	$.fn.crossFade = $.fn.fadeOver;


})(jQuery);
