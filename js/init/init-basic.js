(function($) {
	var projectInit = {
		immediate: function() {

			$.each('abbr article aside audio bdi canvas data datalist details dialog figcaption figurecaption figure footer header hgroup main mark meter nav output progress section summary template time video'.split(' '), function(i, name) {
				document.createElement(name);
			});

			$.webshims.debug = window.jspackager.devmode ? 'noCombo' : undefined;
			$.webshims.setOptions({
				extendNative: false,
				track: {
					override: !Modernizr.mobile
				}
			});

			/**
			 * define modules in capable browsers (IE8+)
			 */
			if (Modernizr.boxSizing || Modernizr['display-table'] || Modernizr.mediaqueries || Modernizr.geoloaction) {

				loadModule.define('enhanced-modules', '../__enhanced-modules.js');
				// loadModule.define('enhanced-map-modules', '../__enhanced-map-modules.js');

				if (!$.enableMediaQuery && !Modernizr.mediaqueries && !window.respond) {
					loadModule.define('mediaqueries-modules', '../extra/jquery.mediaqueries.js');
				}

			}

			$.webshims.polyfill('es5 mediaelement');

			loadModule('mediaqueries-modules');
			loadModule('enhanced-modules');
			// loadModule('enhanced-map-modules');

		},
		domReadyOnce: function() {

		},
		everyDomReady: function(context) {}
	};

	/* starters */
	projectInit.immediate();
	$(projectInit.domReadyOnce);
	$(function() {
		projectInit.everyDomReady(document);
	});
})(jQuery);
