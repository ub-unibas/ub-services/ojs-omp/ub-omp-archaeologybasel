# archaeologyBasel
OJS3.2.1 default child theme im Unibas-Stil. 

_Angesteuerte Seiten jeweils mit Ctrl+F5 neu laden!_

## Produktives OJS 3.2.1-x

https://ub-ojs3.ub.unibas.ch/

## Sandbox

Alle Änderungen sind erlaubt, Inhalte werden nicht auf produktives OJS übertragen!  

Sandbox OJS 3.2.1-1: https://ub-eterna-test.ub.unibas.ch/  
Sandbox OJS 3.1.2-4: https://ub-ojs-test.ub.unibas.ch (VERALTET - WIRD NICHT MEHR UNTERHALTEN!)

Login Sandbox Adminpanel:
- Name: admin
- Passwort: xmas19!

## Staging
Auf der Staging-Instanz läuft jeweils die neue/nächste Version von OJS/eterna, mit (explizit einzuspielenden) aktuellen Daten aus der produktiven Umgebung.

https://ub-ojs-dev.ub.unibas.ch/

Die credentials sind mit denjenigen der produktiven Umgebung identisch.

## Workflow
Priorisierung und Pendenzen: https://github.com/MHindermann/archaeologyBasel/projects  
Probleme, Vorschläge, Ideen, etc. bitte unter https://github.com/MHindermann/archaeologyBasel/issues sammeln!

## Wiki
Anleitungen etc. sind im Wiki zu finden und abzulegen: https://github.com/MHindermann/archaeologyBasel/wiki
