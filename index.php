<?php

/**
 * @defgroup plugins_themes_archaeology_basel Sub theme plugin
 */

/**
 * @file plugins/themes/archaeologyBasel/index.php
 *
 * Copyright (c) 2014-2020 Simon Fraser University
 * Copyright (c) 2003-2020 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @ingroup plugins_themes_archaeology_basel
 * @brief Wrapper for Basel archaeology sub theme plugin.
 *
 */

require_once('BaselArchSubThemePlugin.inc.php');

return new BaselArchSubThemePlugin();

?>
